#ifndef ImpressionTrackerHanlder_H
#define ImpressionTrackerHanlder_H

#include <memory>
#include <string>
class AdServContext;
class EventLogRetriever;
class EntityToModuleStateStats;
class ImpressionTrackerMainProcessor;
#include "Poco/Net/HTTPServerRequest.h"
#include "Poco/Net/HTTPServerResponse.h"
#include "Poco/Net/HTTPRequestHandler.h"
#include "Poco/URI.h"

class ImpressionTrackerHanlder : public Poco::Net::HTTPRequestHandler {

public:

ImpressionTrackerMainProcessor* impressionTrackerMainProcessor;
EventLogRetriever* eventLogRetriever;
EntityToModuleStateStats* entityToModuleStateStats;

ImpressionTrackerHanlder(ImpressionTrackerMainProcessor* ImpressionTrackerMainProcessor);

void handleRequest(Poco::Net::HTTPServerRequest & request,
																			Poco::Net::HTTPServerResponse & response);

std::string process(AdServContext* adservRequest);

void readQueryParams(Poco::URI::QueryParameters &params,
																					std::string &transactionId);

};

#endif
