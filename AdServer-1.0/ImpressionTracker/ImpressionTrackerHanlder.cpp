#include "ImpressionTrackerMainProcessor.h"
#include "GUtil.h"
#include "JsonUtil.h"
#include "HttpUtil.h"
#include "Poco/URI.h"
#include "ImpressionTrackerHanlder.h"
#include "AdServContext.h"
#include "EventLogRetriever.h"
#include "ImpressionTrackerMainProcessor.h"
#include "EntityToModuleStateStats.h"

ImpressionTrackerHanlder::ImpressionTrackerHanlder(ImpressionTrackerMainProcessor* impressionTrackerMainProcessor) {
        this->impressionTrackerMainProcessor = impressionTrackerMainProcessor;
}

void ImpressionTrackerHanlder::handleRequest(Poco::Net::HTTPServerRequest &request,
                                             Poco::Net::HTTPServerResponse &response) {

        //cout("T: Request from " + StringUtil::toStr(request.clientAddress().toString()));
        auto context  = std::make_shared<AdServContext> ();
        std::string uriStr = request.getURI ();
        Poco::URI uri (uriStr);
        Poco::URI::QueryParameters params = uri.getQueryParameters ();

        std::string transactionId;
        readQueryParams(params, transactionId);
        eventLogRetriever->retrieveEventLogFromTransactionId(transactionId, context);

        context->adInteractionType = "imptrack";

        MLOG(10) << "event :  " << context->eventLog->toJson ();

        process(context.get());
        std::ostream &ostr = response.send ();
        ostr << "";
}

std::string ImpressionTrackerHanlder::process(AdServContext* adservRequest) {
        return impressionTrackerMainProcessor->process(adservRequest);
}


void ImpressionTrackerHanlder::readQueryParams(Poco::URI::QueryParameters &params,
                                               std::string &transactionId) {
        for (Poco::URI::QueryParameters::const_iterator it = params.begin ();
             it != params.end (); ++it) {

                if (it->first.compare ("transactionId") == 0) {
                        transactionId = it->second;
                        MLOG(3) << "transactionId is set to " << transactionId;
                }
        }
}
