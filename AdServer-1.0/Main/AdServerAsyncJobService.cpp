#include "GUtil.h"
#include "CollectionUtil.h"
#include "SignalHandler.h"
#include "StackTraceUtil.h"
#include "ConfigService.h"
#include "AdServerAsyncJobService.h"
#include "Poco/Net/ServerSocket.h"
#include "Poco/Net/HTTPServer.h"
#include "Poco/Util/HelpFormatter.h"
#include "ApplicationContext.h"
#include "DateTimeUtil.h"
#include "CampaignCacheService.h"
#include "TargetGroupCacheService.h"
#include "ConcurrentHashMap.h"
#include "BeanFactory.h"
#include "TargetGroup.h"
#include "Campaign.h"
#include "ApplicationContext.h"
#include "ConfigService.h"
#include "ConverterUtil.h"
#include <thread>
#include <boost/exception/all.hpp>
#include "AdInteractionRecordingModule.h"
#include "MySqlDataMover.h"
#include "MySqlHourlyCompressor.h"
#include "ImpServMainProcessor.h"
#include "DataReloadService.h"
#include "EntityToModuleStateStatsPersistenceService.h"
#include "AdServerHandlerFactory.h"
#include "DateTimeMacro.h"
#include "DataClient.h"
#include "ImpressionRecordingModule.h"
#include "ConfirmedWinRequest.h"
#include "HttpUtil.h"

AdServerAsyncJobService::AdServerAsyncJobService() {
        lastTimeDataWasReloadedInSeconds = DateTimeUtil::getNowInSecond();

}

AdServerAsyncJobService::~AdServerAsyncJobService() {

}

void AdServerAsyncJobService::reloadCaches(std::shared_ptr<gicapods::AtomicBoolean> interruptedFlag) {
        do {
                try {
                        isReloadingDataInProgress->setValue(true);

                        //sleep 4 seconds for all bid requests to be processed
                        //or it will segfault
                        //we sleep for 0 second to make the code fail hard if there is any concurrency issues
                        gicapods::Util::sleepViaBoost (_L_, 0);


                        NULL_CHECK(entityToModuleStateStats);
                        entityToModuleStateStats->addStateModuleForEntity ("reloadCaches",
                                                                           "AdServerAsyncJobService",
                                                                           "ALL");

                        LOG(INFO) << "reloading the caches";

                        // now we reload caches and targetgroups
                        dataReloadService->reloadDataViaHttp();
                        auto targetgroups = dataReloadService->beanFactory->targetGroupCacheService->getAllCurrentTargetGroups();
                        auto size = StringUtil::toStr(targetgroups->size());
                        entityToModuleStateStats->addStateModuleForEntity ("#CurrentTargetGroups:" + size,
                                                                           "AdServerAsyncJobService",
                                                                           "ALL");
                        lastTimeDataWasReloadedInSeconds = DateTimeUtil::getNowInSecond();
                } catch (std::exception const &e) {
                        LOG(ERROR) << "error happening when reloading caches  " << boost::diagnostic_information (e);
                        //TODO go red and show in dashboard that this is red
                }

                isReloadingDataInProgress->setValue(false);
                if(!interruptedFlag->getValue()) {
                        int reloadingCacheInterval = ConverterUtil::convertTo<int> (
                                configService->get ("reloadCachesIntervalInSecond"));
                        gicapods::Util::sleepViaBoost (_L_, reloadingCacheInterval);
                }
        } while (!interruptedFlag->getValue());

}

void AdServerAsyncJobService::runEveryMinute() {
        while(true) {
                LOG(INFO)<<"runEveryMinute is called";
                try {

                        numberOfRealAdServingInLastMinute->setValue(0);
                        sendConfirmedWinsToBidders();
                        int oldnessOfReloadedData = DateTimeUtil::getNowInSecond() - lastTimeDataWasReloadedInSeconds;
                        if (oldnessOfReloadedData > 300) {
                                entityToModuleStateStats->addStateModuleForEntity(
                                        "DATA_IS_TOO_OLD",
                                        "AdServerAsyncJobService",
                                        EntityToModuleStateStats::all,
                                        EntityToModuleStateStats::exception
                                        );
                        }

                        mySqlImpressionMover->inserterThread();
                        int diff = DateTimeUtil::getNowInSecond() - mySqlImpressionMover->lastTimeDataMoved;
                        if (diff > 400) {
                                LOG(ERROR)<< "data was moved "<<diff<< " seocnds ago. is ImpressionCompressor cron job runnnig";
                                entityToModuleStateStats->addStateModuleForEntity(
                                        "IMPRESSION_DIDNNOT_MOVE",
                                        "AdServerAsyncJobService",
                                        "ALL",
                                        EntityToModuleStateStats::exception);
                                isMySqlImpressionMoverHealthy->setValue(false);
                        } else {
                                isMySqlImpressionMoverHealthy->setValue(true);
                        }

                } catch(...) {
                        LOG(ERROR) << " exception in run every minute thread";
                        entityToModuleStateStats->addStateModuleForEntity(
                                "EXCEPTION_IN_RUN_EVERY_MINUTE",
                                "AdServerAsyncJobService",
                                "ALL",
                                EntityToModuleStateStats::exception);
                }


                gicapods::Util::sleepViaBoost (_L_, 60);
        }

}

void AdServerAsyncJobService::sendConfirmedWinsToBidders() {


        auto map = bidderToConfirmedWins->moveMap();
        for (auto iter = map->begin ();
             iter != map->end ();
             iter++) {
                try {
                        auto bidderCallbackServletUrl = iter->first;
                        LOG(ERROR) << "request to send to bidder for confirmWins : " << iter->second->toJson();
                        LOG(ERROR) << "bidderCallbackServletUrl : " << bidderCallbackServletUrl;
                        std::string confirmResponse = HttpUtil::sendPostRequest (
                                bidderCallbackServletUrl,
                                iter->second->toJson(),
                                1000,
                                10);
                } catch (std::exception& e) {
                        gicapods::Util::showStackTrace(&e);
                }
        }
        bidderToConfirmedWins->clear();
}

void AdServerAsyncJobService::runEvery10Seconds() {
        while(true) {
                try {
                        //we need to recrd events in cassandra as soon as possible in case
                        //there is any conversion,  conversionDecider should have access to this
                        //transaction
                        impressionRecordingModule->adInteractionRecordingModule->recordEvents();
                } catch(...) {
                        LOG(ERROR) << " exception in run every minute thread";
                        entityToModuleStateStats->addStateModuleForEntity(
                                "EXCEPTION_IN_RUN_runEvery10Seconds",
                                "AdServerAsyncJobService",
                                "ALL",
                                EntityToModuleStateStats::exception);
                }


                gicapods::Util::sleepViaBoost (_L_, 10);
        }

}

void AdServerAsyncJobService::runEveryHour() {
        while(true) {
                try {

                        mySqlMetricCompressor->compressThread();

                        numberOfPSAServingInLastHour->setValue(0);
                } catch(...) {
                        LOG(ERROR) << " exception in run every hour thread";
                }


                gicapods::Util::sleepViaBoost (_L_, 3600);
        }

}

void AdServerAsyncJobService::runEveryTenMinutes() {
        while(true) {
                try {

                        numberOfExceptionsInLast10Min->setValue(0);
                } catch(...) {
                        LOG(ERROR) << " exception in runEveryTenMinutes thread";
                }

                gicapods::Util::sleepViaBoost (_L_, 600);
        }

}
void AdServerAsyncJobService::startAsyncThreads() {


        // auto targetgroups = this->dataReloadService->beanFactory->targetGroupCacheService->getAllEntities();

        // //we reload data once when application is booting up
        this->reloadCaches(std::make_shared<gicapods::AtomicBoolean>(true));



        std::shared_ptr<gicapods::AtomicBoolean> interruptedFlag = std::make_shared<gicapods::AtomicBoolean>(false);
        // data reload happens via a forcereloadcaches request coming from dataMaster
        std::thread startReloadingCacheThread (&AdServerAsyncJobService::reloadCaches, this, interruptedFlag);
        startReloadingCacheThread.detach ();


        std::thread runEveryMinuteThread (&AdServerAsyncJobService::runEveryMinute, this);
        runEveryMinuteThread.detach ();

        std::thread runEveryTenMinutesThread (&AdServerAsyncJobService::runEveryTenMinutes, this);
        runEveryTenMinutesThread.detach ();

        std::thread runEvery10SecondsThread (&AdServerAsyncJobService::runEvery10Seconds, this);
        runEvery10SecondsThread.detach ();

        std::thread runEveryHourThread (&AdServerAsyncJobService::runEveryHour, this);
        runEveryHourThread.detach ();


        entityToModuleStateStatsPersistenceService->startThread();
}
