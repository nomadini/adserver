#ifndef AdServApplicationContext_H
#define AdServApplicationContext_H


#include "AtomicBoolean.h"
#include <memory>
#include <string>
#include <vector>
#include <fstream>



class AdServApplicationContext {

private:

public:

static std::shared_ptr<std::ofstream> getBidEventsAppender();
static std::shared_ptr<gicapods::AtomicBoolean> getThreadsInterruptedFlag();
static std::string getTransactionIdConstant();


AdServApplicationContext();

void info(const std::string& str);

};



#endif
