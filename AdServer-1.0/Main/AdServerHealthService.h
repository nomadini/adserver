#ifndef AdServerHealthService_H
#define AdServerHealthService_H

namespace gicapods { class ConfigService; }
class EntityToModuleStateStats;
#include "AtomicBoolean.h"
#include "AtomicLong.h"
class AdServerHealthService {


public:
EntityToModuleStateStats* entityToModuleStateStats;
std::shared_ptr<gicapods::AtomicBoolean> isImpressionServerHandlerHealthy;
std::shared_ptr<gicapods::AtomicBoolean> isMySqlImpressionMoverHealthy;
std::shared_ptr<gicapods::AtomicBoolean> isSendingPacingInfoModuleHealthy;
std::shared_ptr<gicapods::AtomicBoolean> isReloadingDataInProgress;
std::shared_ptr<gicapods::AtomicLong> numberOfExceptionsInLast10Min;
std::shared_ptr<gicapods::AtomicLong> numberOfRealAdServingInLastMinute;
std::shared_ptr<gicapods::AtomicLong> numberOfPSAServingInLastHour;
std::shared_ptr<gicapods::AtomicLong> acceptableNumberOfExceptionsInLastTenMintues;
AdServerHealthService();
bool isAdServerHealthy();
virtual ~AdServerHealthService();
};



#endif
