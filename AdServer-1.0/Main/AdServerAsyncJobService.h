#ifndef AdServerAsyncJobService_H
#define AdServerAsyncJobService_H

#include "ApplicationContext.h"
#include <memory>
#include <string>
#include "Poco/Util/Application.h"
#include "Poco/Util/Option.h"
#include "Poco/Util/OptionSet.h"
#include "Poco/Util/ServerApplication.h"

#include <memory>
#include <string>
class CampaignCacheService;
class TargetGroupCacheService;
class CreativeCacheService;
namespace gicapods {
template <class K, class V>
class ConcurrentHashMap;
class ConfigService;
}

class ImpServMainProcessor;
class DataReloadService;
class EntityToModuleStateStatsPersistenceService;
class AdServerHandlerFactory;
class ImpressionRecordingModule;

#include <boost/thread.hpp>
#include <thread>
#include "AtomicLong.h"
#include "DateTimeMacro.h"

class EntityToModuleStateStats;
class ImpressionEventDto;

class MetricDbRecord;
template<class T>
class MySqlHourlyCompressor;
template<class T>
class MySqlDataMover;
class AdServerAsyncJobService;
class ConfirmedWinRequest;


class AdServerAsyncJobService :
        public std::enable_shared_from_this<AdServerAsyncJobService> {
public:

int deliveryInfoStatsToPacerInterval;
std::shared_ptr<gicapods::AtomicBoolean> isSendingPacingInfoModuleHealthy;
std::shared_ptr<gicapods::AtomicBoolean> isImpressionServerHandlerHealthy;
std::shared_ptr<gicapods::AtomicBoolean> isMySqlImpressionMoverHealthy;
std::shared_ptr<gicapods::AtomicLong> numberOfExceptionsInLast10Min;
std::shared_ptr<gicapods::AtomicLong> numberOfRealAdServingInLastMinute;
std::shared_ptr<gicapods::AtomicLong> numberOfPSAServingInLastHour;
TimeType lastTimeDataWasReloadedInSeconds;
gicapods::ConfigService* configService;
CampaignCacheService* campaignCacheService;
MySqlHourlyCompressor<MetricDbRecord>* mySqlMetricCompressor;
MySqlDataMover<ImpressionEventDto>* mySqlImpressionMover;
TargetGroupCacheService* targetGroupCacheService;
CreativeCacheService* creativeCacheService;

DataReloadService* dataReloadService;
EntityToModuleStateStatsPersistenceService* entityToModuleStateStatsPersistenceService;

EntityToModuleStateStats* entityToModuleStateStats;
std::shared_ptr<gicapods::AtomicBoolean> isReloadingDataInProgress;
std::shared_ptr<gicapods::AtomicBoolean> interruptedFlag;
ImpressionRecordingModule* impressionRecordingModule;

std::shared_ptr<gicapods::ConcurrentHashMap<std::string,
                                            ConfirmedWinRequest > > bidderToConfirmedWins;

void reloadCaches(std::shared_ptr<gicapods::AtomicBoolean> interruptedFlag);

AdServerAsyncJobService();

virtual ~AdServerAsyncJobService();

void sendConfirmedWinsToBidders();
void startAsyncThreads();
void runEveryTenMinutes();
void runEveryMinute();
void runEvery10Seconds();
void runEveryHour();

private:
bool _helpRequested;
};

#endif
