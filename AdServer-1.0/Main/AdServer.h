#ifndef ADSERVER_H
#define ADSERVER_H

#include <memory>
#include <string>
#include "Poco/Util/Application.h"
#include "Poco/Util/Option.h"
#include "Poco/Util/OptionSet.h"
#include "Poco/Util/ServerApplication.h"
#include <memory>
#include <string>
class CampaignCacheService;
class TargetGroupCacheService;
class CreativeCacheService;
namespace gicapods { class ConfigService; }

#include <boost/thread.hpp>
#include <thread>
class ApplicationContext;
class ImpServMainProcessor;
class DataReloadService;
class AdServerHandlerFactory;
class EntityToModuleStateStatsPersistenceService;
class AdServerAsyncJobService;
class EntityToModuleStateStats;
class BeanFactory;

class AdServer;


class AdServer : public std::enable_shared_from_this<AdServer>,  public Poco::Util::ServerApplication {
public:

gicapods::ConfigService* configService;
std::unique_ptr<AdServerHandlerFactory> adServerHandlerFactory;
std::unique_ptr<AdServerAsyncJobService> adServerAsyncJobService;
std::unique_ptr<BeanFactory> beanFactory;

AdServer();

virtual ~AdServer();

std::string propertyFileName;

int main(const std::vector<std::string>& args);

void readProperties();

private:
bool _helpRequested;
};

#endif
