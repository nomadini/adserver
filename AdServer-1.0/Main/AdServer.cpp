#include "GUtil.h"
#include "CollectionUtil.h"
#include "SignalHandler.h"
#include "StackTraceUtil.h"
#include "ConfigService.h"
#include "AdServer.h"
#include "Poco/Net/ServerSocket.h"
#include "Poco/Net/HTTPServer.h"
#include "Poco/ThreadPool.h"
#include "Poco/Util/HelpFormatter.h"
#include "ApplicationContext.h"
#include "CampaignCacheService.h"
#include "TargetGroupCacheService.h"
#include "ApplicationContext.h"
#include "ConfigService.h"
#include "ConverterUtil.h"
#include "BeanFactory.h"
#include <thread>
#include <boost/exception/all.hpp>
#include "ApplicationContext.h"
#include "ImpServMainProcessor.h"
#include "DataReloadService.h"
#include "AdServerHandlerFactory.h"
#include "EntityToModuleStateStatsPersistenceService.h"
#include "AdServerAsyncJobService.h"
#include "PocoHttpServer.h"

AdServer::AdServer() {
}

AdServer::~AdServer() {

}

int AdServer::main(const std::vector<std::string> &args) {

        adServerAsyncJobService->startAsyncThreads();

        auto srv = PocoHttpServer::createHttpServer(configService, adServerHandlerFactory.get());

        // start the HTTPServer
        srv->start ();
        // wait for CTRL-C or kill
        waitForTerminationRequest ();
        // Stop the HTTPServer
        srv->stop ();
        return Application::EXIT_OK;
}
