#include "AdServerHealthService.h"
#include "EntityToModuleStateStats.h"
#include "GUtil.h"
AdServerHealthService::AdServerHealthService() {

}
bool AdServerHealthService::isAdServerHealthy() {


        //TODO : if number of impression served less than 10 per minute or some configurable threshold
        // send the bidder to slow bid mode, because something might be wrong
        if (numberOfRealAdServingInLastMinute->getValue() >=500) {
                LOG(ERROR) << "showing too many impressions : " << numberOfRealAdServingInLastMinute->getValue();
                entityToModuleStateStats->addStateModuleForEntity ("SHOWING_TOO_MANY_IMPS",
                                                                   "AdServerHealthService",
                                                                   "ALL",
                                                                   EntityToModuleStateStats::exception);
                return false;
        }

        LOG_EVERY_N(INFO, 100) << " numberOfRealAdServingInLastMinute : " << numberOfRealAdServingInLastMinute->getValue();

        if (numberOfPSAServingInLastHour->getValue() >= 10) {
                LOG(ERROR) << "numberOfPSAServingInLastHourIsTooHigh";
                entityToModuleStateStats->addStateModuleForEntity ("numberOfPSAServingInLastHourIsTooHigh",
                                                                   "AdServerHealthService",
                                                                   "ALL",
                                                                   EntityToModuleStateStats::exception);
                return false;
        }

        if (!isImpressionServerHandlerHealthy->getValue()) {
                entityToModuleStateStats->addStateModuleForEntity ("isImpressionServerHandlerHealthyNotHealthy",
                                                                   "AdServerHealthService",
                                                                   "ALL",
                                                                   EntityToModuleStateStats::exception);
                LOG(ERROR) << "ImpressionServerHanler IS NOT Healthy";
                return false;
        }

        if (!isMySqlImpressionMoverHealthy->getValue()) {
                entityToModuleStateStats->addStateModuleForEntity ("MySqlImpressionMoverIsNotHealthy",
                                                                   "AdServerHealthService",
                                                                   "ALL",
                                                                   EntityToModuleStateStats::exception);
                LOG(ERROR) << "isMySqlImpressionMoverHealthy IS NOT Healthy";
                return false;
        }
        if (!isSendingPacingInfoModuleHealthy->getValue()) {
                LOG(ERROR) << "sendingPacingInfo IS NOT Healthy";
                entityToModuleStateStats->addStateModuleForEntity ("isSendingPacingInfoModuleNotHealthy",
                                                                   "AdServerHealthService",
                                                                   "ALL",
                                                                   EntityToModuleStateStats::exception);
                return false;
        }

        if (numberOfExceptionsInLast10Min->getValue() >= acceptableNumberOfExceptionsInLastTenMintues->getValue()) {
                LOG(ERROR) << "adserver is facing too many exceptions";
                entityToModuleStateStats->addStateModuleForEntity ("tooManyExceptionsInLastMinute",
                                                                   "AdServerHealthService",
                                                                   "ALL",
                                                                   EntityToModuleStateStats::exception);
                return false;
        }

        entityToModuleStateStats->addStateModuleForEntity ("IsHealthy",
                                                           "AdServerHealthService",
                                                           "ALL");
        return true;
}

AdServerHealthService::~AdServerHealthService() {

}
