
#include "AdHistoryModule.h"
#include "AdServResponseModule.h"
#include "ClientIdModule.h"
#include "CookiesReaderModule.h"
#include "CookiesWriterModule.h"
#include "ImpressionRecordingModule.h"

#include "Status.h"
#include "Encoder.h"
#include "GUtil.h"
#include "AtomicLong.h"
#include "AdServContext.h"
#include "ClickTrackerMainProcessor.h"
#include "ClientIdModule.h"
#include "CampaignCacheService.h"
#include "AdServContext.h"
#include "EventLog.h"
#include "CreativeCacheService.h"
ClickTrackerMainProcessor::ClickTrackerMainProcessor() {

}

std::string ClickTrackerMainProcessor::process(AdServContext* context) {

        for (std::vector<std::shared_ptr<AdServerModule> >::iterator it = modules.begin();
             it != modules.end(); ++it) {
                MLOG(10) << "running" << (*it)->getName() << " module";
                (*it)->process(context);
        }
        // ClickPersistentModule;
        // redirectModule
        return context->finalCreativeContent;

}

ClickTrackerMainProcessor::~ClickTrackerMainProcessor() {
}
