#ifndef ClickTrackerHandler_H
#define ClickTrackerHandler_H

#include <memory>
#include <string>
class AdServContext;
class ClickTrackerMainProcessor;
class EventLogRetriever;
class EntityToModuleStateStats;
#include "Poco/Net/HTTPServerRequest.h"
#include "Poco/Net/HTTPServerResponse.h"
#include "Poco/Net/HTTPRequestHandler.h"
#include "Poco/URI.h"
class ClickTrackerHandler : public Poco::Net::HTTPRequestHandler {

public:

ClickTrackerMainProcessor* clickTrackerMainProcessor;
EventLogRetriever* eventLogRetriever;
EntityToModuleStateStats* entityToModuleStateStats;
ClickTrackerHandler(ClickTrackerMainProcessor* ClickTrackerMainProcessor);

void handleRequest(Poco::Net::HTTPServerRequest & request,
																			Poco::Net::HTTPServerResponse & response);

std::string process(AdServContext* adservRequest);

void readQueryParams(
								Poco::URI::QueryParameters &params,
								std::string &transactionId,
								std::string &ctrackUrl);
};

#endif
