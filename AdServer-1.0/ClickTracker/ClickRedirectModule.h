#ifndef ClickRedirectModule_H
#define ClickRedirectModule_H


#include "Status.h"
#include "AdServerModule.h"

#include <memory>
#include <string>
class AdServContext;
class ClickRedirectModule : public AdServerModule {

private:

public:

EntityToModuleStateStats* entityToModuleStateStats;

ClickRedirectModule(EntityToModuleStateStats* entityToModuleStateStats);

virtual std::string getName();

virtual void process(AdServContext* context);

virtual ~ClickRedirectModule();
};




#endif
