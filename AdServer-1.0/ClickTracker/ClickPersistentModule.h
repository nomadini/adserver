#ifndef ClickPersistentModule_H
#define ClickPersistentModule_H


#include "Status.h"
#include "AdServerModule.h"

#include <memory>
#include <string>
class AdServContext;
class AdHistoryModule;
class AdInteractionRecordingModule;
class ClickPersistentModule : public AdServerModule {

private:

public:
EntityToModuleStateStats* entityToModuleStateStats;
AdInteractionRecordingModule* adInteractionRecordingModule;
AdHistoryModule* adHistoryModule;
ClickPersistentModule(EntityToModuleStateStats* entityToModuleStateStats);

virtual std::string getName();

virtual void process(AdServContext* context);

virtual ~ClickPersistentModule();

};




#endif
