
#include "Status.h"
#include "GUtil.h"
#include "ClickPersistentModule.h"
#include "JsonUtil.h"
#include "EventLog.h"
#include "Poco/Net/HTTPCookie.h"
#include "Poco/Net/NameValueCollection.h"
#include "AdServContext.h"
#include "AdHistoryModule.h"
#include "AdInteractionRecordingModule.h"
ClickPersistentModule::ClickPersistentModule(EntityToModuleStateStats* entityToModuleStateStats) {
								this->entityToModuleStateStats = entityToModuleStateStats;

}

std::string ClickPersistentModule::getName() {
								return "ClickPersistentModule";
}

void ClickPersistentModule::process(AdServContext* context) {
								context->adInteractionType = EventLog::EVENT_TYPE_CLICK;
								adHistoryModule->process(context);

								auto eventLog = context->eventLog;
								eventLog->eventType = EventLog::EVENT_TYPE_CLICK;
								eventLog->sourceEventId = eventLog->eventId;
								adInteractionRecordingModule->enqueueEventForPersistence(eventLog);
}

ClickPersistentModule::~ClickPersistentModule() {
}
