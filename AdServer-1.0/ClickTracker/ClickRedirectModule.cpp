
#include "Status.h"
#include "GUtil.h"
#include "StringUtil.h"
#include "ClickRedirectModule.h"
#include "JsonUtil.h"
#include "Poco/Net/HTTPCookie.h"
#include "Poco/Net/NameValueCollection.h"
#include "AdServContext.h"
ClickRedirectModule::ClickRedirectModule(EntityToModuleStateStats* entityToModuleStateStats) {
								this->entityToModuleStateStats = entityToModuleStateStats;
}

std::string ClickRedirectModule::getName() {
								return "ClickRedirectModule";
}

void ClickRedirectModule::process(AdServContext* context) {
//doesnt do anything now, but keep it for now
}

ClickRedirectModule::~ClickRedirectModule() {

}
