#include "ClickTrackerMainProcessor.h"
#include "GUtil.h"
#include "JsonUtil.h"
#include "HttpUtil.h"
#include "ClickTrackerHandler.h"
#include "AdServContext.h"
#include "ClickTrackerMainProcessor.h"
#include "EventLogRetriever.h"
#include "EntityToModuleStateStats.h"

ClickTrackerHandler::ClickTrackerHandler(ClickTrackerMainProcessor* clickTrackerMainProcessor) {
        this->clickTrackerMainProcessor = clickTrackerMainProcessor;
}

void ClickTrackerHandler::handleRequest(Poco::Net::HTTPServerRequest &request,
                                        Poco::Net::HTTPServerResponse &response) {

        //cout("T: Request from " + StringUtil::toStr(request.clientAddress().toString()));
        HttpUtil::printQueryParamsOfRequest (request);
        HttpUtil::printRequestProperties (request);

        auto context  = std::make_shared<AdServContext> ();
        std::string uriStr = request.getURI ();
        Poco::URI uri (uriStr);
        Poco::URI::QueryParameters params = uri.getQueryParameters ();

        std::string transactionId;
        std::string ctrackUrl;
        readQueryParams(params, transactionId, ctrackUrl);
        eventLogRetriever->retrieveEventLogFromTransactionId(transactionId, context);


        MLOG(10) << "event :  " << context->eventLog->toJson ();
        process(context.get());

        response.redirect(ctrackUrl,
                          Poco::Net::HTTPResponse::HTTPStatus::HTTP_FOUND);
}


std::string ClickTrackerHandler::process(AdServContext* context) {
        return clickTrackerMainProcessor->process(context);
}

void ClickTrackerHandler::readQueryParams(Poco::URI::QueryParameters &params,
                                          std::string &transactionId,
                                          std::string &ctrackUrl
                                          ) {
        for (Poco::URI::QueryParameters::const_iterator it = params.begin ();
             it != params.end (); ++it) {

                if (it->first.compare ("transactionId") == 0) {
                        transactionId = it->second;
                        MLOG(3) << "transactionId is set to " << transactionId;
                } else if (it->first.compare ("ctrack") == 0) {
                        ctrackUrl = it->second;
                        MLOG(3) << "ctrackUrl is set to " << ctrackUrl;
                }
        }
}
