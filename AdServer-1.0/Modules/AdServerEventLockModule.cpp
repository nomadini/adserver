
#include "Status.h"
#include "GUtil.h"
#include "AdServerEventLockModule.h"
#include "JsonUtil.h"
#include "Poco/Net/HTTPCookie.h"
#include "Poco/Net/NameValueCollection.h"
#include "AdServContext.h"
#include "EventLock.h"
AdServerEventLockModule::AdServerEventLockModule(
								EntityToModuleStateStats* entityToModuleStateStats,
								std::string eventType,
								AeroCacheService<EventLock>* eventLockPersistenceService) {
								this->entityToModuleStateStats =  entityToModuleStateStats;
								this->eventType = eventType;
								this->eventLockPersistenceService = eventLockPersistenceService;

}

std::string AdServerEventLockModule::getName() {
								return "AdServerEventLockModule";
}

void AdServerEventLockModule::process(AdServContext* context) {

								auto eventLockOriginal = std::make_shared<EventLock>(context->eventLog->eventId, eventType);

								int cacheResultStatus = 0;
								auto eventLock = eventLockPersistenceService->readDataOptional(eventLockOriginal, cacheResultStatus);
								if (cacheResultStatus == AerospikeDriver::CACHE_MISS) {
																//event is locked
																eventLockPersistenceService->putDataInCache(eventLockOriginal);
																entityToModuleStateStats->addStateModuleForEntity (
																								"Locking_Event_" + eventType,
																								"AdServerEventLockModule",
																								"ALL");

								} else {
																//event is already locked
																entityToModuleStateStats->addStateModuleForEntity (
																								"Repeated_Event_" + eventType,
																								"AdServerEventLockModule",
																								"ALL");
																if (StringUtil::equalsIgnoreCase(
																												eventType,
																												EventLog::EVENT_TYPE_IMPRESSION)) {
																								context->eventLog->scrubType = EventLog::SCRUB_TYPE_REPEATED_IMPRESSION;
																} else if (StringUtil::equalsIgnoreCase(
																																			eventType,
																																			EventLog::EVENT_TYPE_CLICK)) {
																								context->eventLog->scrubType = EventLog::SCRUB_TYPE_REPEATED_CLICK;
																} else {
																								EXIT("unknown eventType "+ eventType);
																}

								}

}

AdServerEventLockModule::~AdServerEventLockModule() {
}
