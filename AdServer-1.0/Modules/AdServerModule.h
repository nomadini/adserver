#ifndef AdServerModule_H
#define AdServerModule_H


#include "Status.h" //this is needed in every module
#include "AdServContext.h"
#include "StringUtil.h"//used a lot
#include <memory>
#include <string>
#include "EntityToModuleStateStats.h"

class AdServerModule;


class AdServerModule {

private:

public:

AdServerModule();

void logAdServerModuleStarted();

virtual void process(AdServContext* context)=0;
virtual std::string getName()=0;
virtual ~AdServerModule();
};

#endif
