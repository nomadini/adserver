
#include "Status.h"
#include "GUtil.h"
#include "AdHistoryCassandraService.h"
#include "AdHistoryModule.h"
#include "ConverterUtil.h"
#include "JsonUtil.h"
#include "AdServContext.h"
#include "EventLog.h"
#include "Device.h"
AdHistoryModule::AdHistoryModule(EntityToModuleStateStats* entityToModuleStateStats,
																																	AdHistoryCassandraService* adHistoryCassandraService) {
								this->entityToModuleStateStats = entityToModuleStateStats;
								this->adHistoryCassandraService = adHistoryCassandraService;
}


std::string AdHistoryModule::getName() {
								return "AdHistoryModule";
}

void AdHistoryModule::process(AdServContext* context) {
								writeImpressionToDb(context);
}
void AdHistoryModule::writeImpressionToDb(AdServContext* context) {

								assertAndThrow(context->eventLog->creativeId> 0);
								assertAndThrow(context->eventLog->targetGroupId > 0 );

								// assertAndThrow(context->eventLog->publisherId> 0); // TODO : send this later

								//TODO : do we need to save the eventLog as an impression event at the end of ImpressionHandler pipeline
								std::shared_ptr<AdEntry> adEntry = std::make_shared<AdEntry>(
																context->eventLog->eventId,
																context->adInteractionType);

								adEntry->creativeId = context->eventLog->creativeId;
								adEntry->targetGroupId = context->eventLog->targetGroupId;
								adEntry->publisherId = context->eventLog->publisherId;
								assertAndThrow(!context->eventLog->device->getDeviceId().empty());
								assertAndThrow(!context->eventLog->device->getDeviceType().empty());
								adHistoryCassandraService->saveAdInteraction(context->eventLog->device, adEntry);
}

AdHistoryModule::~AdHistoryModule() {

}
