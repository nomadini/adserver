#ifndef AdServContext_H
#define AdServContext_H



#include "Status.h"
#include "TargetGroupTypeDefs.h"
class TargetGroup;
#include "CreativeTypeDefs.h"
class Creative;
#include "CampaignTypeDefs.h"
class StringUtil;
#include <memory>
#include <string>
#include "Poco/Net/NameValueCollection.h"
#include <tbb/concurrent_hash_map.h>
#include "Poco/Net/HTTPServerRequest.h"
#include "Poco/Net/HTTPServerResponse.h"
#include "DateTimeMacro.h"
class Device;
class EventLog;
class AdServContext;



/**
   adserv context has all the eventLog info that comes from a bid,
   it also has impression specific fields.
 */
class AdServContext {

private:

public:
/**
 * this has all the information regarding the bid that we are showing impression for.
 */
std::shared_ptr<EventLog> eventLog;
std::shared_ptr<Creative> chosenCreative;
std::string finalCreativeContent;
std::string wrappedCreativeWithClickUrl;
std::string adInteractionType;

std::shared_ptr<Device> device;
std::string creativePalette;

std::string requestHandlerName;
std::string requestURI;
std::string lastModuleRunInPipeline;

Poco::Net::NameValueCollection cookieMapIncoming;   // this is what will be written in response. it includes all the incoming cookies
Poco::Net::NameValueCollection cookieMapOutgoing;
Poco::Net::HTTPServerRequest* httpRequestPtr;
Poco::Net::HTTPServerResponse* httpResponsePtr;

std::shared_ptr<gicapods::Status> status;

AdServContext();

std::string toString();

static std::shared_ptr<AdServContext> fromJson(std::string json);

std::string toJson();

virtual ~AdServContext();

static std::string createRandomTransactionId();

};

#endif
