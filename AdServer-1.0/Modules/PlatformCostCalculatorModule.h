#ifndef PlatformCostCalculatorModule_H
#define PlatformCostCalculatorModule_H


#include <memory>
#include <string>
#include <vector>

class EntityToModuleStateStats;
class EventLog;
class CampaignCacheService;

class PlatformCostCalculatorModule {

private:

public:

EntityToModuleStateStats* entityToModuleStateStats;
CampaignCacheService* campaignCacheService;

PlatformCostCalculatorModule();

void calculateCost(std::shared_ptr<EventLog> eventLog);

virtual ~PlatformCostCalculatorModule();

};




#endif
