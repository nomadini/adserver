
#include "Status.h"
#include "GUtil.h"
#include "CookiesReaderModule.h"
#include "JsonUtil.h"
#include "Poco/Net/HTTPCookie.h"
#include "Poco/Net/NameValueCollection.h"
#include "AdServContext.h"
CookiesReaderModule::CookiesReaderModule(EntityToModuleStateStats* entityToModuleStateStats) {
								this->entityToModuleStateStats = entityToModuleStateStats;

}

std::string CookiesReaderModule::getName() {
								return "CookiesReaderModule";
}

void CookiesReaderModule::process(AdServContext* context) {

								readIncomingCookies(context);


}
/**
 * this module reads all the cookies and put it in adservContext , thats all it does
 */
void CookiesReaderModule::readIncomingCookies(AdServContext* context) {
								assertAndThrow(context->httpRequestPtr != NULL);
								context->httpRequestPtr->getCookies(context->cookieMapIncoming);
								MLOG(10)<<"context->cookieMapIncoming.size()  "<<context->cookieMapIncoming.size();
}

void CookiesReaderModule::readCookies() {

}

CookiesReaderModule::~CookiesReaderModule() {
}
