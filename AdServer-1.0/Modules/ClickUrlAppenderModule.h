#ifndef ClickUrlAppenderModule_H
#define ClickUrlAppenderModule_H


#include "Status.h"
class ApplicationContext;

#include <memory>
#include <string>
class AdServContext;
#include "AdServerModule.h"
class CreativeCacheService;
class RandomUtil;

class ClickUrlAppenderModule : public AdServerModule {

private:

public:
static std::shared_ptr<RandomUtil> getRandomizer();
std::string clickServerIpAddress;
std::string clickServerPort;
EntityToModuleStateStats* entityToModuleStateStats;

ClickUrlAppenderModule(EntityToModuleStateStats* entityToModuleStateStats);

std::string getName();

virtual void process(AdServContext* context);

std::string buildClickUrl(AdServContext* context);

virtual ~ClickUrlAppenderModule();

};



#endif
