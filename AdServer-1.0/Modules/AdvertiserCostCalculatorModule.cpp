#include "GUtil.h"
#include "AdvertiserCostCalculatorModule.h"
#include "StringUtil.h"
#include "DateTimeUtil.h"
#include "EntityToModuleStateStats.h"
#include "EventLog.h"
#include "CampaignCacheService.h"
#include "Campaign.h"
#include "AdvertiserCostModel.h"

AdvertiserCostCalculatorModule::AdvertiserCostCalculatorModule() {

}

void AdvertiserCostCalculatorModule::calculateCost(std::shared_ptr<EventLog> eventLog) {
        auto campaign = campaignCacheService->findByEntityId(eventLog->campaignId);
        long allExtraPercCosts = 0;
        for(auto advertiserImpBasedPercentageCostEntry : campaign->advCostModel->advertiserImpBasedPercentageCost->costs) {
                assertAndThrow(advertiserImpBasedPercentageCostEntry->percentageValue >=0);
                assertAndThrow(advertiserImpBasedPercentageCostEntry->percentageValue <=100);
                long extraCost = advertiserImpBasedPercentageCostEntry->percentageValue  * (eventLog->platformCost / 100);
                allExtraPercCosts += extraCost;
        }

        long allFixedPercCosts = 0;
        for(auto advertiserImpBasedFixedCostEntry : campaign->advCostModel->advertiserImpBasedFixedCost->costs) {
                allFixedPercCosts += advertiserImpBasedFixedCostEntry->fixedValue;
        }

        eventLog->advertiserCost = allFixedPercCosts + allExtraPercCosts + eventLog->platformCost;

}
AdvertiserCostCalculatorModule::~AdvertiserCostCalculatorModule() {

}
