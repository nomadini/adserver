#ifndef ImpressionRecordingModule_H
#define ImpressionRecordingModule_H


#include "Status.h"
#include "AdServerModule.h"
class EventLogCassandraService;
#include <memory>
#include <string>
class AdServContext;
class TargetGroupCacheService;
class EventLogCassandraService;
#include <tbb/concurrent_queue.h>
class EventLog;
class PlatformCostCalculatorModule;
class AdvertiserCostCalculatorModule;
class AdInteractionRecordingModule;
class CampaignCacheService;

class ImpressionRecordingModule : public AdServerModule {

private:

public:

std::unique_ptr<PlatformCostCalculatorModule> platformCostCalculatorModule;
std::unique_ptr<AdvertiserCostCalculatorModule> advertiserCostCalculatorModule;

EntityToModuleStateStats* entityToModuleStateStats;
AdInteractionRecordingModule* adInteractionRecordingModule;
ImpressionRecordingModule(
        EntityToModuleStateStats* entityToModuleStateStats,
        AdInteractionRecordingModule* adInteractionRecordingModule);

virtual std::string getName();

virtual void process(AdServContext* context);

void calculateThePlatformCost(std::shared_ptr<EventLog> eventLog);
void calculateTheAdvertiserCost(std::shared_ptr<EventLog> eventLog);

virtual ~ImpressionRecordingModule();

};





#endif
