#ifndef ImpressionTrackerAppenderModule_H
#define ImpressionTrackerAppenderModule_H


#include "Status.h"
#include "AdServerModule.h"

#include <memory>
#include <string>
class AdServContext;
class RandomUtil;
#include "AtomicLong.h"

class ImpressionTrackerAppenderModule : public AdServerModule {

private:

public:
static std::shared_ptr<RandomUtil> getRandomizer();
std::string impressionServerIpAddress;
EntityToModuleStateStats* entityToModuleStateStats;

ImpressionTrackerAppenderModule(EntityToModuleStateStats* entityToModuleStateStats);

virtual std::string getName();

virtual void process(AdServContext* context);


virtual ~ImpressionTrackerAppenderModule();
};




#endif
