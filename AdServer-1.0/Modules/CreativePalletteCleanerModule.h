#ifndef CreativePalletteCleanerModule_H
#define CreativePalletteCleanerModule_H


#include "Status.h"
#include "AdServerModule.h"

#include <memory>
#include <string>
class AdServContext;
#include "AtomicLong.h"

class CreativePalletteCleanerModule: public AdServerModule{

private:

public:

	EntityToModuleStateStats* entityToModuleStateStats;

	CreativePalletteCleanerModule(EntityToModuleStateStats* entityToModuleStateStats);

	virtual std::string getName();

	virtual void process(AdServContext* context);

	virtual ~CreativePalletteCleanerModule() ;
};




#endif
