
#include "Status.h"
#include "GUtil.h"
#include "StringUtil.h"
#include "CookiesWriterModule.h"
#include "JsonUtil.h"
#include "Poco/Net/HTTPCookie.h"
#include "Poco/Net/NameValueCollection.h"
#include "AdServContext.h"
CookiesWriterModule::CookiesWriterModule(EntityToModuleStateStats* entityToModuleStateStats) {
								this->entityToModuleStateStats = entityToModuleStateStats;
}

std::string CookiesWriterModule::getName() {
								return "CookiesWriterModule";
}

void CookiesWriterModule::process(AdServContext* context) {

								writeOutGoingCookies(context);

}

void CookiesWriterModule::writeOutGoingCookies(AdServContext* context) {
								assertAndThrow(context->httpResponsePtr != NULL);
								Poco::Net::HTTPCookie cookieOut(context->cookieMapOutgoing);
								context->httpResponsePtr->addCookie(cookieOut);

}

CookiesWriterModule::~CookiesWriterModule() {

}
