#ifndef ConfirmWinCounterModule_H
#define ConfirmWinCounterModule_H


#include "Status.h"
#include "ApplicationContext.h"

#include <memory>
#include <string>
class AdServContext;
#include "AdServerModule.h"
class CreativeCacheService;
class ConfirmedWinRequest;
namespace gicapods {
template <class K, class V>
class ConcurrentHashMap;
class ConfigService;
}
class ConfirmWinCounterModule : public AdServerModule {

private:

public:
EntityToModuleStateStats* entityToModuleStateStats;

std::shared_ptr<gicapods::ConcurrentHashMap<std::string,
                                            ConfirmedWinRequest > > bidderToConfirmedWins;

ConfirmWinCounterModule(EntityToModuleStateStats* entityToModuleStateStats);

std::string getName();

virtual void process(AdServContext* context);

virtual ~ConfirmWinCounterModule();

};



#endif
