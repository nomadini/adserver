
#include "Status.h"
#include "GUtil.h"
#include "ImpressionTrackerAppenderModule.h"
#include "JsonUtil.h"
#include "StringUtil.h"
#include "Poco/Net/HTTPCookie.h"
#include "Poco/Net/NameValueCollection.h"
#include "ApplicationContext.h"
#include "RandomUtil.h"
#include "AdServContext.h"
#include "EventLog.h"

ImpressionTrackerAppenderModule::ImpressionTrackerAppenderModule(EntityToModuleStateStats* entityToModuleStateStats) {
								this->entityToModuleStateStats = entityToModuleStateStats;
								// http://67.205.132.35/impressions/1.html;
								impressionServerIpAddress = "67.205.132.35:9981";
}

std::shared_ptr<RandomUtil> ImpressionTrackerAppenderModule::getRandomizer() {
								static std::shared_ptr<RandomUtil> randomizer =
																std::make_shared<RandomUtil> (1000000);
								return randomizer;
}


std::string ImpressionTrackerAppenderModule::getName() {
								return "ImpressionTrackerAppenderModule";
}

void ImpressionTrackerAppenderModule::process(AdServContext* context) {
								std::string imptrackerUrl =
																"<img src=\"IMP_TRAKER_SERVER\" border=\"0\" width=\"1\" height=\"1\">";

								imptrackerUrl = StringUtil::replaceString(
																imptrackerUrl,
																"IMP_TRAKER_SERVER",
																"http://"+impressionServerIpAddress+"/queue0-imptrk?transactionId=__TRN_ID__&CACHE_BUSTER=__RND__");

								imptrackerUrl = StringUtil::replaceString(imptrackerUrl, StringUtil::toStr(AdServApplicationContext::getTransactionIdConstant()), context->eventLog->eventId);
								imptrackerUrl = StringUtil::replaceString(
																imptrackerUrl,
																StringUtil::toStr("__RND__"),
																StringUtil::toStr(getRandomizer()->randomNumberV2()));
								context->creativePalette = StringUtil::replaceString(context->creativePalette, "____NOMADINI_IMPRESSION_TRACKER____", imptrackerUrl);
}




ImpressionTrackerAppenderModule::~ImpressionTrackerAppenderModule() {

}
