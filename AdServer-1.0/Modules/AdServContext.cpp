
#include "GUtil.h"
#include "Status.h"
#include "TargetGroup.h"
#include "Creative.h"
#include "AdHistory.h"
#include "Campaign.h"
#include "StringUtil.h"
#include "AdServContext.h"
#include "JsonUtil.h"
#include "ConverterUtil.h"
#include "DateTimeUtil.h"
#include "StringUtil.h"
#include "EventLog.h"
#include "AdHistory.h"
AdServContext::AdServContext() {

        status = std::make_shared<gicapods::Status>();
        creativePalette =
                "   ____NOMADINI_AD_TAG____     "
                "   ____NOMADINI_IMPRESSION_TRACKER____     "
                "   ____NOMADINI_PIXEL_MATCHING_URL____     ";
}

std::string AdServContext::toString() {
        return "";
}


AdServContext::~AdServContext() {

}

std::string AdServContext::createRandomTransactionId() {
        return StringUtil::random_string(12) + StringUtil::toStr("_")
               + StringUtil::toStr(DateTimeUtil::getNowInMicroSecond());
}
