#ifndef CookiesWriterModule_H
#define CookiesWriterModule_H


#include "Status.h"
#include "AdServerModule.h"

#include <memory>
#include <string>
class AdServContext;
class CookiesWriterModule: public AdServerModule{

private:

public:

	EntityToModuleStateStats* entityToModuleStateStats;

	CookiesWriterModule(EntityToModuleStateStats* entityToModuleStateStats);

	virtual std::string getName();

	virtual void process(AdServContext* context);

	void writeOutGoingCookies(AdServContext* context);

	virtual ~CookiesWriterModule() ;
};




#endif
