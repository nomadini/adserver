
#include "Status.h"
#include "ApplicationContext.h"
#include "RandomUtil.h"
#include "GUtil.h"
#include "AdServResponseModule.h"
#include "JsonUtil.h"
#include "ConverterUtil.h"
#include "CreativeCacheService.h"
#include "Creative.h"
#include "AdServContext.h"
#include "Device.h"
#include "EventLog.h"
AdservResponseModule::AdservResponseModule(
        EntityToModuleStateStats* entityToModuleStateStats,
        CreativeCacheService* creativeCacheService) {
        this->entityToModuleStateStats = entityToModuleStateStats;
        this->creativeCacheService = creativeCacheService;

}

std::string AdservResponseModule::getName() {
        return "AdServResponsModule";
}

void AdservResponseModule::process(AdServContext* context) {

        try {
                context->finalCreativeContent = prepareCreativeAdTag(context);
        } catch (std::exception const&  e) {
                LOG(ERROR)<<"chosen creative doesn't exist, id :"<<
                        context->eventLog->creativeId;
                throwEx(
                        "there was no creative with id in creative map : "
                        + context->eventLog->creativeId);
        }
}

std::string AdservResponseModule::prepareCreativeAdTag(AdServContext* context) {

        auto creativePalleteWithCreative = StringUtil::replaceString(context->creativePalette,
                                                                     "____NOMADINI_AD_TAG____",
                                                                     context->wrappedCreativeWithClickUrl);

        if (StringUtil::contains(creativePalleteWithCreative, "____NOMADINI")) {
                LOG(ERROR) << "error in creativePalleteWithCreative : "<<
                        creativePalleteWithCreative;
                throwEx("some macros are still left in creative pallete");
        }

        return creativePalleteWithCreative;
}

AdservResponseModule::~AdservResponseModule() {

}
