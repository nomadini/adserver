#ifndef ClientIdModule_H
#define ClientIdModule_H


#include "Status.h"
#include "AdServerModule.h"
#include "rapidjson/writer.h"
#include "rapidjson/stringbuffer.h"
#include "AdHistory.h"
class DeviceFeatureHistory;

#include <memory>
#include <string>
class AdServContext;
class ClientIdModule : public AdServerModule {

private:

public:
EntityToModuleStateStats* entityToModuleStateStats;
ClientIdModule(EntityToModuleStateStats* entityToModuleStateStats);

virtual std::string getName();

virtual void process(AdServContext* context);

void getClientId(AdServContext* context);


virtual ~ClientIdModule();

};




#endif
