
#include "Status.h"
#include "GUtil.h"
#include "CreativePalletteCleanerModule.h"
#include "JsonUtil.h"
#include "StringUtil.h"
#include "Poco/Net/HTTPCookie.h"
#include "Poco/Net/NameValueCollection.h"
#include "AdServContext.h"
#include "EntityToModuleStateStats.h"
#include "EventLog.h"

CreativePalletteCleanerModule::CreativePalletteCleanerModule(EntityToModuleStateStats* entityToModuleStateStats) {
								this->entityToModuleStateStats = entityToModuleStateStats;
}

std::string CreativePalletteCleanerModule::getName() {
								return "CreativePalletteCleanerModule";
}

void CreativePalletteCleanerModule::process(AdServContext* context) {

								if (context->eventLog->biddingOnlyForMatchingPixel == false) {
																entityToModuleStateStats->addStateModuleForEntity("ERASE_PIXEL_MATCHING_MACRO", "CreativePalletteCleanerModule", context->eventLog->exchangeName);
																context->creativePalette = StringUtil::replaceString(context->creativePalette, "____NOMADINI_PIXEL_MATCHING_URL____", "");
								}
}


CreativePalletteCleanerModule::~CreativePalletteCleanerModule() {

}
