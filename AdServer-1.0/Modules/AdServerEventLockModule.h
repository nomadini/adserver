#ifndef AdServerEventLockModule_H
#define AdServerEventLockModule_H


#include "Status.h"
#include "AdServerModule.h"
#include "AeroCacheService.h"

#include <memory>
#include <string>
class AdServContext;
class EventLock;
class AdServerEventLockModule : public AdServerModule {

private:
std::string eventType;
AeroCacheService<EventLock>* eventLockPersistenceService;

EntityToModuleStateStats* entityToModuleStateStats;
public:
AdServerEventLockModule(
        EntityToModuleStateStats* entityToModuleStateStats,
        std::string eventType,
        AeroCacheService<EventLock>* eventLockPersistenceService);

virtual std::string getName();

virtual void process(AdServContext* context);

virtual ~AdServerEventLockModule();

};




#endif
