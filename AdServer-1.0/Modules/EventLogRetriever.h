#ifndef EventLogRetriever_H
#define EventLogRetriever_H

#include "Status.h"
#include "AdServerModule.h"
#include "AeroCacheService.h"

#include <memory>

class EventLog;
#include <string>
class AdServContext;
class AdHistoryCassandraService;
class EventLogRetriever {

private:

public:
EntityToModuleStateStats* entityToModuleStateStats;
AeroCacheService<EventLog>* realTimeEventLogCacheService;
EventLogRetriever(EntityToModuleStateStats* entityToModuleStateStats);

virtual std::string getName();

void retrieveEventLogFromTransactionId(std::string transactionId, std::shared_ptr<AdServContext> context);

virtual ~EventLogRetriever();

};




#endif
