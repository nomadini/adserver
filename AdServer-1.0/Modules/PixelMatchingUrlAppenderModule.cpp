
#include "Status.h"
#include "GUtil.h"
#include "PixelMatchingUrlAppenderModule.h"
#include "JsonUtil.h"
#include "StringUtil.h"
#include "Poco/Net/HTTPCookie.h"
#include "Poco/Net/NameValueCollection.h"
#include "AdServContext.h"
#include "EventLog.h"

PixelMatchingUrlAppenderModule::PixelMatchingUrlAppenderModule(EntityToModuleStateStats* entityToModuleStateStats) {
								this->entityToModuleStateStats = entityToModuleStateStats;
}

std::string PixelMatchingUrlAppenderModule::getName() {
								return "PixelMatchingUrlAppenderModule";
}

void PixelMatchingUrlAppenderModule::process(AdServContext* context) {

								if (context->eventLog->biddingOnlyForMatchingPixel == true) {
																entityToModuleStateStats->addStateModuleForEntity("APPEND_PIXEL_MATCHING_URL", "PixelMatchingUrlAppenderModule", context->eventLog->exchangeName);
																numberOfImpressionsDoneForPixelMatchingPerHour->increment();
																buildPixelMatchingUrl(context);

								} else {
																entityToModuleStateStats->addStateModuleForEntity("SKIPPING_PIXEL_MATCHING_URL", "PixelMatchingUrlAppenderModule", context->eventLog->exchangeName);
								}
}

void PixelMatchingUrlAppenderModule::buildPixelMatchingUrl(AdServContext* context) {
								//this is really dependent on the exchanges , so for every exchange we should do it differently
								if (StringUtil::equalsIgnoreCase("google", context->eventLog->exchangeName)) {
																context->creativePalette = StringUtil::replaceString(context->creativePalette, "____NOMADINI_PIXEL_MATCHING_URL____", googlePixelMatchTag);

								}

}


PixelMatchingUrlAppenderModule::~PixelMatchingUrlAppenderModule() {

}
