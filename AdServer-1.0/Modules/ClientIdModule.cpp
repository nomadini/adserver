
#include "Status.h"
#include "rapidjson/writer.h"
#include "rapidjson/stringbuffer.h"
#include "AdHistory.h"
#include "DeviceFeatureHistory.h"
#include "GUtil.h"
#include "Device.h"
#include "StringUtil.h"
#include "ClientIdModule.h"
#include "AdServContext.h"
#include "JsonUtil.h"
ClientIdModule::ClientIdModule(EntityToModuleStateStats* entityToModuleStateStats) {
								this->entityToModuleStateStats = entityToModuleStateStats;
}



std::string ClientIdModule::getName() {
								return "ClientIdModule";
}

void ClientIdModule::process(AdServContext* context) {

								getClientId(context);
}

void ClientIdModule::getClientId(AdServContext* context) {

								if(context->cookieMapIncoming.has(StringUtil::toStr("nomadiniDeviceId"))) {
																context->device = std::make_shared<Device>(
																								context->cookieMapIncoming.get(StringUtil::toStr("nomadiniDeviceId")),
																								""
																								);
																entityToModuleStateStats->addStateModuleForEntity (
																								"KNOWN_VISITOR_SEEN",
																								"ClientIdModule",
																								"ALL");

								} else  {
																std::string nomadiniDeviceId = Device::createStandardDeviceId();
																context->cookieMapOutgoing.add(StringUtil::toStr("nomadiniDeviceId"), nomadiniDeviceId);
																context->device = std::make_shared<Device>(nomadiniDeviceId,"");
																entityToModuleStateStats->addStateModuleForEntity (
																								"NEW_VISITOR_SEEN",
																								"ClientIdModule",
																								"ALL");
								}
}

ClientIdModule::~ClientIdModule() {

}
