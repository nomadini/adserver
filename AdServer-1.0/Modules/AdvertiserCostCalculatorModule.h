#ifndef AdvertiserCostCalculatorModule_H
#define AdvertiserCostCalculatorModule_H


#include <memory>
#include <string>
#include <vector>

class EventLog;
class EntityToModuleStateStats;
class CampaignCacheService;
class AdvertiserCostCalculatorModule {

private:

public:

EntityToModuleStateStats* entityToModuleStateStats;
CampaignCacheService* campaignCacheService;

AdvertiserCostCalculatorModule();

void calculateCost(std::shared_ptr<EventLog> eventLog);

virtual ~AdvertiserCostCalculatorModule();

};

#endif
