
#include "Status.h"
#include "GUtil.h"
#include "AdHistoryCassandraService.h"
#include "EventLogRetriever.h"
#include "ConverterUtil.h"
#include "JsonUtil.h"
#include "AdServContext.h"
#include "EventLog.h"
#include "AeroCacheService.h"
EventLogRetriever::EventLogRetriever(EntityToModuleStateStats* entityToModuleStateStats) {
								this->entityToModuleStateStats = entityToModuleStateStats;
}


std::string EventLogRetriever::getName() {
								return "EventLogRetriever";
}

void EventLogRetriever::retrieveEventLogFromTransactionId(
								std::string transactionId,
								std::shared_ptr<AdServContext> context) {
								if (transactionId.empty()) {
																entityToModuleStateStats->addStateModuleForEntity (
																								"EMPTY_TRANSACTION_ID",
																								"ImpressionServHandler",
																								"ALL",
																								EntityToModuleStateStats::exception);
																throwEx("bad request, eventLog is not passed");
								}
								auto inputEventLog = std::make_shared<EventLog>(
																transactionId,
																EventLog::EVENT_TYPE_IMPRESSION,
																EventLog::TARGETING_TYPE_IGNORE_FOR_NOW);
								int cacheResultStatus = 0;
								auto event = realTimeEventLogCacheService->readDataOptional(inputEventLog, cacheResultStatus);
								if (event == nullptr) {
																entityToModuleStateStats->addStateModuleForEntity(
																								"EVENT_LOG_NOT_FOUND",
																								"EventLogRetriever",
																								"ALL",
																								EntityToModuleStateStats::exception);
																throwEx("eventLog is not found : eventLog Id : " + transactionId);
								}
								entityToModuleStateStats->addStateModuleForEntity(
																"EVENT_LOG_FOUND",
																"EventLogRetriever",
																"ALL");
								context->eventLog = event;
}

EventLogRetriever::~EventLogRetriever() {

}
