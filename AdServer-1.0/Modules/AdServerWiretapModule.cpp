#include "Status.h"

#include "CollectionUtil.h"
#include "AdServContext.h"

#include "EntityToModuleStateStats.h"
#include "AdServerWiretapModule.h"
#include "JsonUtil.h"

#include "Status.h"
#include "ModulesToStatePercentages.h"
#include "EntityToModuleStateStats.h"
#include "TargetGroup.h"
#include "TargetGroupCacheService.h"
#include "EventLog.h"
#include "AdServContext.h"
#include "WiretapCassandraService.h"

std::string AdServerWiretapModule::getName() {
        return "AdServerWiretapModule";
}

AdServerWiretapModule::AdServerWiretapModule(
        EntityToModuleStateStats* entityToModuleStateStats) : Object(__FILE__) {
        this->entityToModuleStateStats = entityToModuleStateStats;

}

void AdServerWiretapModule::process(AdServContext* context) {

        if(!wiretapImpressionServeResponseSampler->isPartOfSample()) {
                return;
        }

        auto wiretap = std::make_shared<Wiretap>();
        wiretap->eventId = context->eventLog->eventId;
        wiretap->appName = "AdServer";
        // std::string appVersion;
        // std::string appHost;
        assertAndThrow(!context->requestHandlerName.empty());
        assertAndThrow(!context->requestURI.empty());
        wiretap->moduleName = context->requestHandlerName;
        wiretap->request = context->requestURI;
        wiretap->response = context->finalCreativeContent;
        wiretapCassandraService->record(wiretap);

}
