
#include "Status.h"
#include "ApplicationContext.h"
#include "RandomUtil.h"
#include "GUtil.h"
#include "AdServResponseModule.h"
#include "CreativeCacheService.h"
#include "Creative.h"
#include "EventLog.h"
#include "AdServContext.h"
#include "ApplicationContext.h"
#include "ClickUrlAppenderModule.h"
ClickUrlAppenderModule::ClickUrlAppenderModule(
        EntityToModuleStateStats* entityToModuleStateStats) {
        this->entityToModuleStateStats = entityToModuleStateStats;
        clickServerIpAddress = "67.205.132.35";
        clickServerPort = "9981";

}

std::shared_ptr<RandomUtil> ClickUrlAppenderModule::getRandomizer() {
        static std::shared_ptr<RandomUtil> randomizer =
                std::make_shared<RandomUtil> (100000);
        return randomizer;
}

std::string ClickUrlAppenderModule::getName() {
        return "ClickUrlAppenderModule";
}

void ClickUrlAppenderModule::process(AdServContext* context) {
        NULL_CHECK(context->chosenCreative);
        auto clickUrl = buildClickUrl(context);

        context->wrappedCreativeWithClickUrl =
                StringUtil::replaceStringCaseInsensitive(context->chosenCreative->getAdTag(),
                                                         "${CLICKURL}",
                                                         clickUrl);
        MLOG(3)<<"context->wrappedCreativeWithClickUrl : "<<
                context->wrappedCreativeWithClickUrl;
}

std::string ClickUrlAppenderModule::buildClickUrl(AdServContext* context) {
        std::string clickUrl =
                "http://"
                +clickServerIpAddress +
                ":" + clickServerPort +
                "/queue0-clk?transactionId=__TRN_ID__&CACHE_BUSTER=__RND__&ctrack=";
        clickUrl = StringUtil::replaceString(clickUrl, StringUtil::toStr(AdServApplicationContext::getTransactionIdConstant()), context->eventLog->eventId);
        clickUrl = StringUtil::replaceString(clickUrl, StringUtil::toStr("__RND__"), StringUtil::toStr(
                                                     getRandomizer()->randomNumberV2()));
        return clickUrl;
}

ClickUrlAppenderModule::~ClickUrlAppenderModule() {

}
