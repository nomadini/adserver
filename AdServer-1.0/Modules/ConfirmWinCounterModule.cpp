
#include "Status.h"
#include "GUtil.h"
#include "JsonUtil.h"
#include "ConverterUtil.h"
#include "ConfirmedWinRequest.h"
#include "AdServContext.h"
#include "ConfirmWinCounterModule.h"
#include "ConcurrentHashMap.h"
#include "IntWrapper.h"
#include "EventLog.h"
#include "HashMap.h"

ConfirmWinCounterModule::ConfirmWinCounterModule(
        EntityToModuleStateStats* entityToModuleStateStats) {
        this->entityToModuleStateStats = entityToModuleStateStats;


}

std::string ConfirmWinCounterModule::getName() {
        return "AdServResponsModule";
}

void ConfirmWinCounterModule::process(AdServContext* context) {


        auto requestPayload = bidderToConfirmedWins->getOrCreate(context->eventLog->bidderCallbackServletUrl);
        auto numberOfWins = *requestPayload->targetGroupIdWins->getOrCreate(context->eventLog->targetGroupId);
        //TODO : fix this later
        numberOfWins.increment();
        LOG_EVERY_N(ERROR, 100) << "confirmation : tgId " <<
                context->eventLog->targetGroupId <<
                "numberOfWins : "<<numberOfWins.getValue();

}

ConfirmWinCounterModule::~ConfirmWinCounterModule() {

}
