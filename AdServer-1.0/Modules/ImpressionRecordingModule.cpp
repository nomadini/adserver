#include "Status.h"
#include "GUtil.h"
#include "ImpressionRecordingModule.h"

#include "DateTimeUtil.h"
#include "ConverterUtil.h"
#include "JsonUtil.h"
#include "JsonArrayUtil.h"
#include "StringUtil.h"
#include "AdInteractionRecordingModule.h"
#include "AdServContext.h"
#include "AdvertiserCostCalculatorModule.h"
#include "PlatformCostCalculatorModule.h"
#include "Device.h"
#include "CampaignCacheService.h"
#include "EventLog.h"

ImpressionRecordingModule::ImpressionRecordingModule(
        EntityToModuleStateStats* entityToModuleStateStats,
        AdInteractionRecordingModule* adInteractionRecordingModule) {
        this->entityToModuleStateStats = entityToModuleStateStats;
        this->adInteractionRecordingModule = adInteractionRecordingModule;
}

std::string ImpressionRecordingModule::getName() {
        return "ImpressionRecordingModule";
}

void ImpressionRecordingModule::process(AdServContext* context) {

        std::shared_ptr<EventLog> eventLog = context->eventLog;
        eventLog->eventType = EventLog::EVENT_TYPE_IMPRESSION;
        eventLog->sourceEventId = eventLog->eventId;
        eventLog->eventId = EventLog::createRandomTransactionId();
        assertAndThrow(!eventLog->eventType.empty());
        platformCostCalculatorModule->calculateCost (eventLog);
        advertiserCostCalculatorModule->calculateCost (eventLog);
        adInteractionRecordingModule->enqueueEventForPersistence(eventLog);
}

ImpressionRecordingModule::~ImpressionRecordingModule() {

}
