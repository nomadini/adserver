#ifndef PixelMatchingUrlAppenderModule_H
#define PixelMatchingUrlAppenderModule_H


#include "Status.h"
#include "AdServerModule.h"

#include <memory>
#include <string>
class AdServContext;
#include "AtomicLong.h"

class PixelMatchingUrlAppenderModule: public AdServerModule{

private:

public:

	EntityToModuleStateStats* entityToModuleStateStats;
	std::shared_ptr<gicapods::AtomicLong> numberOfImpressionsDoneForPixelMatchingPerHour;
	static constexpr const char* googlePixelMatchTag =
	"<img src=\"http://cm.g.doubleclick.net/pixel?google_nid=1234&google_cm\" />";

	PixelMatchingUrlAppenderModule(EntityToModuleStateStats* entityToModuleStateStats);

	virtual std::string getName();

	virtual void process(AdServContext* context);

	void buildPixelMatchingUrl(AdServContext* context);

	virtual ~PixelMatchingUrlAppenderModule() ;
};




#endif
