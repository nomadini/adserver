#ifndef CookiesReaderModule_H
#define CookiesReaderModule_H


#include "Status.h"
#include "AdServerModule.h"

#include <memory>
#include <string>
class AdServContext;
class CookiesReaderModule : public AdServerModule {

private:

public:
EntityToModuleStateStats* entityToModuleStateStats;
CookiesReaderModule(EntityToModuleStateStats* entityToModuleStateStats);

virtual std::string getName();

virtual void process(AdServContext* context);

void readIncomingCookies(AdServContext* context);

void readCookies();

virtual ~CookiesReaderModule();

};




#endif
