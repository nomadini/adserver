#ifndef AdservResponseModule_H
#define AdservResponseModule_H


#include "Status.h"
#include "ApplicationContext.h"

#include <memory>
#include <string>
class AdServContext;
#include "AdServerModule.h"
class CreativeCacheService;

class AdservResponseModule : public AdServerModule {

private:

public:
EntityToModuleStateStats* entityToModuleStateStats;
CreativeCacheService* creativeCacheService;

AdservResponseModule(EntityToModuleStateStats* entityToModuleStateStats,
                     CreativeCacheService* creativeCacheService);

std::string getName();

virtual void process(AdServContext* context);

void buildClickUrl(AdServContext* context);

std::string prepareCreativeAdTag(AdServContext* context);

virtual ~AdservResponseModule();

};



#endif
