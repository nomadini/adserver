#ifndef AdHistoryModule_H
#define AdHistoryModule_H

#include "Status.h"
#include "AdServerModule.h"

#include <memory>
#include <string>
class AdServContext;
class AdHistoryCassandraService;
class AdHistoryModule : public AdServerModule {

private:

public:
EntityToModuleStateStats* entityToModuleStateStats;
AdHistoryCassandraService* adHistoryCassandraService;
AdHistoryModule(EntityToModuleStateStats* entityToModuleStateStats,
																AdHistoryCassandraService* adHistoryCassandraService);

virtual std::string getName();

virtual void process(AdServContext* context);

void writeImpressionToDb(AdServContext* context);

virtual ~AdHistoryModule();

};




#endif
