/*
 * TargetGroupDailyCapsFilter.h
 *
 *  Created on: Sep 4, 2015
 *      Author: mtaabodi
 */

#ifndef AdServerWiretapModule_H_
#define AdServerWiretapModule_H_

#include "Status.h"
#include "TargetGroupTypeDefs.h"
class TargetGroup;
#include "Object.h"
#include "CollectionUtil.h"
class AdServContext;

class EntityToModuleStateStats;
class TargetGroup;
class TargetGroupCacheService;
class WiretapCassandraService;

#include <memory>
#include <string>
#include "AtomicDouble.h"
#include "AtomicLong.h"
#include "LoadPercentageBasedSampler.h"
#include "ConcurrentHashSet.h"
#include "EntityToModuleStateStats.h"
#include "AdServerModule.h"

class AdServerWiretapModule : public AdServerModule, public Object {

private:

public:
WiretapCassandraService* wiretapCassandraService;
std::unique_ptr<LoadPercentageBasedSampler> wiretapImpressionServeResponseSampler;
EntityToModuleStateStats* entityToModuleStateStats;
virtual std::string getName();
AdServerWiretapModule(
        EntityToModuleStateStats* entityToModuleStateStats);

virtual void process(AdServContext* context);
};

#endif
