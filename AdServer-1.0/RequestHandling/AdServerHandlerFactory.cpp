#include "ImpressionServHandler.h"
#include "SignalHandler.h"
#include "StackTraceUtil.h"
#include "ClickTrackerHandler.h"
#include "AdServerHandlerFactory.h"
#include "GUtil.h"

#include "StringUtil.h"

#include "CommonRequestHandlerFactory.h"
;
#include "AdServerHealthService.h"
#include "ImpServMainProcessor.h"
#include "UnknownRequestHandler.h"
#include "TempUtil.h"
#include "AdServStatusHandler.h"
#include "ImpressionServHandler.h"

#include "AdServerHealthService.h"
#include "ClickTrackerMainProcessor.h"
#include "ImpressionTrackerMainProcessor.h"
#include "ImpressionTrackerHanlder.h"
#include "EventLogRetriever.h"

AdServerHandlerFactory::AdServerHandlerFactory(TargetGroupCacheService* targetGroupCacheService,
                                               CreativeCacheService* creativeCacheService,
                                               EntityToModuleStateStats* entityToModuleStateStats) {
        this->targetGroupCacheService = targetGroupCacheService;
        this->creativeCacheService = creativeCacheService;

        this->entityToModuleStateStats = entityToModuleStateStats;
}


Poco::Net::HTTPRequestHandler *AdServerHandlerFactory::createRequestHandler(
        const Poco::Net::HTTPServerRequest &request) {
        try {
                LOG_EVERY_N (ERROR,1000) << "request.getURI() " << request.getURI ();

                if (StringUtil::contains (request.getURI (), "/win")) {
                        MLOG (3) << "win handler was chosen  " << request.getURI ();
                        NULL_CHECK(impServMainProcessor);
                        auto impressionServHandler =  new ImpressionServHandler ();
                        impressionServHandler->impServMainProcessor = impServMainProcessor.get();
                        impressionServHandler->entityToModuleStateStats = entityToModuleStateStats;

                        impressionServHandler->eventLogRetriever = eventLogRetriever.get();
                        impressionServHandler->isImpressionServerHandlerHealthy = isImpressionServerHandlerHealthy;
                        impressionServHandler->numberOfExceptionsInLast10Min = numberOfExceptionsInLast10Min;
                        impressionServHandler->isReloadingDataInProgress = isReloadingDataInProgress;
                        impressionServHandler->numberOfRealAdServingInLastMinute = numberOfRealAdServingInLastMinute;
                        impressionServHandler->numberOfPSAServingInLastHour = numberOfPSAServingInLastHour;
                        return impressionServHandler;
                }  else if (StringUtil::contains (request.getURI (), "/AdServer/status")) {
                        MLOG (3) << "status handler was chosen  " << request.getURI ();
                        auto adServStatusHandler = new AdServStatusHandler (targetGroupCacheService, creativeCacheService);
                        adServStatusHandler->adServerHealthService = adServerHealthService;
                        adServStatusHandler->adServerHealthService->isImpressionServerHandlerHealthy = isImpressionServerHandlerHealthy;
                        adServStatusHandler->entityToModuleStateStats = entityToModuleStateStats;
                        return adServStatusHandler;
                } else if (StringUtil::contains (request.getURI (), "/queue0-clk?")) {
                        MLOG (3) << "click handler was chosen  " << request.getURI ();
                        auto clickTrackerHandler = new ClickTrackerHandler (clickTrackerMainProcessor.get());
                        clickTrackerHandler->eventLogRetriever = eventLogRetriever.get();
                        clickTrackerHandler->entityToModuleStateStats = entityToModuleStateStats;
                        return clickTrackerHandler;
                }  else if (StringUtil::contains (request.getURI (), "/queue0-imptrk?")) {
                        MLOG (3) << "click tracker was chosen  " << request.getURI ();
                        auto impressionTrackerHanlder = new ImpressionTrackerHanlder (impressionTrackerMainProcessor.get());
                        impressionTrackerHanlder->entityToModuleStateStats = entityToModuleStateStats;
                        impressionTrackerHanlder->eventLogRetriever = eventLogRetriever.get();

                        return impressionTrackerHanlder;
                }

                auto commonRequestHandler = commonRequestHandlerFactory->createRequestHandler(request);
                if (commonRequestHandler != nullptr) {
                        return commonRequestHandler;
                }


        } catch(...) {
                gicapods::Util::showStackTrace();
        }

        //TODO : if we get too many UnknownRequestHandler as the final handler we should go red!!!
        return new UnknownRequestHandler (entityToModuleStateStats);

}

AdServerHandlerFactory::~AdServerHandlerFactory() {
}
