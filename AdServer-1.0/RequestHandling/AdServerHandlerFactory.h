#ifndef AdServerHandlerFactory_H
#define AdServerHandlerFactory_H


#include "Poco/Net/HTTPRequestHandlerFactory.h"
#include "Poco/Net/HTTPServerRequest.h"
#include "Poco/Net/HTTPRequestHandler.h"
#include <memory>
#include <string>
#include <unordered_map>
class AdServContext;
class ImpServMainProcessor;
class TargetGroupCacheService;
class CreativeCacheService;
class EntityToModuleStateStats;
#include <boost/thread.hpp>
class CommonRequestHandlerFactory;
class AdServerHandlerFactory;
class EventLog;
class AdServerHealthService;

class ClickTrackerMainProcessor;
class ImpressionTrackerMainProcessor;
class ImpressionTrackerHanlder;
class EventLogRetriever;


#include "AeroCacheService.h";

class AdServerHandlerFactory : public Poco::Net::HTTPRequestHandlerFactory {

public:
std::unique_ptr<ImpServMainProcessor> impServMainProcessor;

std::unique_ptr<ClickTrackerMainProcessor> clickTrackerMainProcessor;
std::unique_ptr<ImpressionTrackerMainProcessor> impressionTrackerMainProcessor;
std::unique_ptr<ImpressionTrackerHanlder> impressionTrackerHanlder;

std::unique_ptr<EventLogRetriever> eventLogRetriever;
std::shared_ptr<gicapods::AtomicBoolean> isReloadingDataInProgress;
std::shared_ptr<gicapods::AtomicLong> numberOfExceptionsInLast10Min;
std::shared_ptr<gicapods::AtomicLong> numberOfRealAdServingInLastMinute;
std::shared_ptr<gicapods::AtomicLong> numberOfPSAServingInLastHour;
EntityToModuleStateStats* entityToModuleStateStats;

TargetGroupCacheService* targetGroupCacheService;
CreativeCacheService* creativeCacheService;
CommonRequestHandlerFactory* commonRequestHandlerFactory;

std::shared_ptr<gicapods::AtomicBoolean> isImpressionServerHandlerHealthy;

AdServerHealthService* adServerHealthService;


AdServerHandlerFactory(TargetGroupCacheService* targetGroupCacheService,
                       CreativeCacheService* creativeCacheService,
                       EntityToModuleStateStats* entityToModuleStateStats);



Poco::Net::HTTPRequestHandler* createRequestHandler(const Poco::Net::HTTPServerRequest& request);

virtual ~AdServerHandlerFactory();
};

#endif
