#include "ImpServMainProcessor.h"
#include "SignalHandler.h"
#include "StackTraceUtil.h"
#include "JsonUtil.h"
#include "EntityToModuleStateStats.h"

#include "Encoder.h"
#include "GUtil.h"
#include "AdServStatusHandler.h"
#include "JsonUtil.h"
#include "HttpUtil.h"
#include "Poco/URI.h"
#include <boost/exception/all.hpp>
#include <tbb/concurrent_hash_map.h>
#include "DateTimeUtil.h"
#include "AdServerStatus.h"
#include "ImpressionServHandler.h"
#include "AppStatusRequest.h"


AdServStatusHandler::AdServStatusHandler(TargetGroupCacheService* targetGroupCacheService,
                                         CreativeCacheService* creativeCacheService) {

        this->targetGroupCacheService = targetGroupCacheService;
        this->creativeCacheService = creativeCacheService;
}

void AdServStatusHandler::handleRequest(Poco::Net::HTTPServerRequest &request,
                                        Poco::Net::HTTPServerResponse &response) {

        MLOG(3) << "processing status request from bidder ";

        auto adserverStatusResponse = determineStatus();
        //TODO : if the exception happens for a campaingId or targetGroupId , send it to bidder
        //so bidder stop using that campaignId or targetGroupId
        std::ostream &ostr = response.send ();
        std::string statsAsStr = adserverStatusResponse->toJson ();
        LOG_EVERY_N(INFO, 10) << "sending status of adserver back to bidder : " << statsAsStr;
        ostr << statsAsStr;
        return;
}

std::string AdServStatusHandler::process(AdServContext* adservRequest) {
        return impServMainProcessor->process (adservRequest);
}

std::shared_ptr<AppStatusRequest> AdServStatusHandler::determineStatus() {
        std::shared_ptr<AppStatusRequest> adserverStatusResponse = std::make_shared<AppStatusRequest> ();


        adserverStatusResponse->status = "GREEN";
        adserverStatusResponse->reason = "ALL_GOOD";

        if(!adServerHealthService->isAdServerHealthy()) {
                adserverStatusResponse->status = "RED";
                adserverStatusResponse->reason = "UNHEALTHY_IMPRESSION_HANLDER";
                entityToModuleStateStats->addStateModuleForEntity ("adserverIsNotHealthy",
                                                                   "ImpressionServHandler",
                                                                   "ALL");
        }

        return adserverStatusResponse;
}
