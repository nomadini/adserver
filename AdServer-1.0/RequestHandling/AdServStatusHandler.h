#ifndef AdServStatusHandler_H
#define AdServStatusHandler_H


#include "ImpServMainProcessor.h"
#include "SignalHandler.h"
#include "StackTraceUtil.h"

class AdServContext;
#include <memory>
#include <string>
#include "Poco/Net/HTTPServerRequest.h"
#include "Poco/Net/HTTPServerResponse.h"
#include "Poco/Net/HTTPRequestHandler.h"
class TargetGroupCacheService;
class CreativeCacheService;
#include "AdServerHealthService.h"
class EntityToModuleStateStats;
#include "AppStatusRequest.h"

class AdServStatusHandler : public Poco::Net::HTTPRequestHandler {

public:
ImpServMainProcessor* impServMainProcessor;
TargetGroupCacheService* targetGroupCacheService;
CreativeCacheService* creativeCacheService;
AdServerHealthService* adServerHealthService;
EntityToModuleStateStats* entityToModuleStateStats;
AdServStatusHandler(TargetGroupCacheService* targetGroupCacheService,
																				CreativeCacheService* creativeCacheService);

void handleRequest(Poco::Net::HTTPServerRequest & request,
																			Poco::Net::HTTPServerResponse & response);

std::shared_ptr<AppStatusRequest> determineStatus();

std::string process(AdServContext* adservRequest);

};

#endif
