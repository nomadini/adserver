
#include "GUtil.h"
#include "CassandraDriverInterface.h"
#include "ApplicationContext.h"
#include "ConfigService.h"
#include "AdServer.h"
#include "GUtil.h"
#include "StringUtil.h"
#include "AdServerEventLockModule.h"
#include "SignalHandler.h"
#include <string>
#include <memory>
#include "TempUtil.h"
#include "TargetGroupCacheService.h"
#include "CampaignCacheService.h"
#include "ImpServMainProcessor.h"

#include "ImpServMainProcessor.h"
#include "ClickTrackerMainProcessor.h"
#include "ImpressionTrackerMainProcessor.h"
#include "ImpressionTrackerHanlder.h"
#include "HttpUtilService.h"
#include "ConfigService.h"
#include "AdServerHandlerFactory.h"
#include "AdHistoryCassandraService.h"

#include "DataReloadService.h"
#include "AdServerAsyncJobService.h"
#include "AdServerModule.h"
#include "AdHistoryModule.h"
#include "ClickPersistentModule.h"
#include "ConcurrentHashMap.h"
#include "ClickRedirectModule.h"
#include "AdServResponseModule.h"
#include "ClientIdModule.h"
#include "CookiesReaderModule.h"
#include "CookiesWriterModule.h"
#include "ImpressionRecordingModule.h"
#include "AtomicLong.h"
#include "AdServContext.h"
#include "ImpServMainProcessor.h"
#include "ClientIdModule.h"
#include "CampaignCacheService.h"
#include "EntityToModuleStateStatsPersistenceService.h"
#include "CommonRequestHandlerFactory.h"

#include <tbb/concurrent_queue.h>

#include "PixelMatchingUrlAppenderModule.h"
#include "CreativePalletteCleanerModule.h"
#include "ImpressionTrackerAppenderModule.h"
#include "AsyncThreadPoolService.h"

#include "ClientDealCacheService.h"
#include "DealCacheService.h"
#include "BeanFactory.h"
#include "AeroCacheService.h"
#include "ClickUrlAppenderModule.h"
#include "AdServerHealthService.h"
#include "EventLogRetriever.h"
#include "AdvertiserCostModel.h"
#include "PlatformCostCalculatorModule.h"
#include "AdServerWiretapModule.h"
#include "AdvertiserCostCalculatorModule.h"
#include "ConfirmedWinRequest.h"
#include "ConfirmWinCounterModule.h"
#include "ServiceFactory.h"

/*
   available macros
   GEO Macros:
   ${LAT}Latitude
   ${LON}Longitude
   ${COUNTRY}Country
   ${STATE}State
   ${DMA}DMA/Metro Code
   ${POSTAL_CODE}US Zip Code
   ${MGRS_10}MGRS 10 based on lat/lon
   ${MGRS_100}MGRS 100 based on lat/lon
   ${MGRS_1000}MGRS 1000 based on lat/lon
   Device ID Macros:
   ${BEST_DEVICE_ID}Most Reliable Device ID
   ${BEST_DEVICE_TYPE}ID Type associated with Best Device macro
   Device ID - Android Specific Macros:
   ${ANDROID_ID}Android ID of device
   ${ANDROID_ID_SHA1}SHA-1 hash of Android ID of device
   ${ANDROID_ID_MD5}MD5 hash of Android ID of device
   ${G_IDFA}Android Advertising ID of device
   Device ID - iOS Specific Macros:
   ${IDFA}IDFA of device
   ${IDFA_SHA1}SHA-1 hash of IDFA of device
   ${IDFA_MD5}MD5 hash of IDFA of device
   Inventory Macros:
   ${PUBLISHER_NAME}URL or App Name
   ${NETWORK}Exchange name
   Entity Macros:
   ${CAMPAIGN_KEY}Bartender Campaign ID
   ${TARGET_GROUP_KEY}Bartender Target Group ID
   ${CREATIVE_KEY}Bartender Creative ID
   Click and Cachebusting Macros:
   ${CLICKURL}Non-Encoded Click Macro
   ${CLICKURLENC}Single Encoded Click Macro
   ${RND}Cachebusting Macro
   Click and Cachebusting Macros (Legacy - Still Supported):
   [CLICK_REDIRECT]Non-Encoded Click Macro
   [CLICK_REDIRECT_ENC]Single-Encoded Click Macro
   [CLICK_REDIRECT_DBENC]Double-Encoded Click Macro
   [TIMESTAMP]Cachebusting Macro

 */
int main(int argc, char **argv) {

        std::string appName = "AdServer";
        TempUtil::configureLogging (appName, argv);
        std::string propertyFileName = "adserver.properties";
        auto beanFactory = std::make_unique<BeanFactory>("SNAPSHOT");
        beanFactory->propertyFileName = propertyFileName;
        beanFactory->commonPropertyFileName = "common.properties";
        beanFactory->appName = appName;

        beanFactory->initializeModules();

        auto serviceFactory = std::make_unique<ServiceFactory>(beanFactory.get());
        serviceFactory->initializeModules();

        auto interruptedFlag = std::make_shared<gicapods::AtomicBoolean>(false);
        auto isImpressionServerHandlerHealthy = std::make_shared<gicapods::AtomicBoolean>(true);
        auto isMySqlImpressionMoverHealthy = std::make_shared<gicapods::AtomicBoolean>(true);
        auto isSendingPacingInfoModuleHealthy = std::make_shared<gicapods::AtomicBoolean>(true);
        MLOG(3) << " starting the adserver :  " << argc << " , argv : " << StringUtil::toStr (*argv);


        auto bidderToConfirmedWins =
                std::make_shared<gicapods::ConcurrentHashMap<std::string, ConfirmedWinRequest > >(
                        beanFactory->entityToModuleStateStats.get()
                        );


        auto confirmWinCounterModule = std::make_shared<ConfirmWinCounterModule>(
                beanFactory->entityToModuleStateStats.get());
        confirmWinCounterModule->bidderToConfirmedWins = bidderToConfirmedWins;


        auto cookiesReaderModule = std::make_shared<CookiesReaderModule>(
                beanFactory->entityToModuleStateStats.get());;


        auto impressionLockModule = std::make_shared<AdServerEventLockModule>(
                beanFactory->entityToModuleStateStats.get(),
                EventLog::EVENT_TYPE_IMPRESSION,
                beanFactory->eventLockPersistenceService.get());

        auto clickLockModule = std::make_shared<AdServerEventLockModule>(
                beanFactory->entityToModuleStateStats.get(),
                EventLog::EVENT_TYPE_CLICK,
                beanFactory->eventLockPersistenceService.get());

        auto clientIdModule = std::make_shared<ClientIdModule>(
                beanFactory->entityToModuleStateStats.get());

        auto adHistoryModule = std::make_shared<AdHistoryModule>(
                beanFactory->entityToModuleStateStats.get(),
                beanFactory->adHistoryCassandraService.get());


        auto clickPersistentModule = std::make_shared<ClickPersistentModule>(
                beanFactory->entityToModuleStateStats.get());

        clickPersistentModule->adInteractionRecordingModule =
                beanFactory->adInteractionRecordingModule.get();

        clickPersistentModule->adHistoryModule = adHistoryModule.get();

        auto platformCostCalculatorModule = std::make_unique<PlatformCostCalculatorModule> ();
        platformCostCalculatorModule->campaignCacheService = beanFactory->campaignCacheService.get();
        platformCostCalculatorModule->entityToModuleStateStats = beanFactory->entityToModuleStateStats.get();

        auto advertiserCostCalculatorModule = std::make_unique<AdvertiserCostCalculatorModule> ();
        advertiserCostCalculatorModule->campaignCacheService = beanFactory->campaignCacheService.get();
        advertiserCostCalculatorModule->entityToModuleStateStats = beanFactory->entityToModuleStateStats.get();


        auto impressionRecordingModule = std::make_shared<ImpressionRecordingModule>(
                beanFactory->entityToModuleStateStats.get(),
                beanFactory->adInteractionRecordingModule.get());


        impressionRecordingModule->platformCostCalculatorModule = std::move(platformCostCalculatorModule);
        impressionRecordingModule->advertiserCostCalculatorModule = std::move(advertiserCostCalculatorModule);

        auto cookiesWriterModule = std::make_shared<CookiesWriterModule>(
                beanFactory->entityToModuleStateStats.get());;

        auto adservResponseModule = std::make_shared<AdservResponseModule>
                                            (beanFactory->entityToModuleStateStats.get(),
                                            beanFactory->creativeCacheService.get());

        auto adServerWiretapModule = std::make_shared<AdServerWiretapModule>
                                             (beanFactory->entityToModuleStateStats.get());

        auto wiretapImpressionServeResponseSampler = std::make_unique<LoadPercentageBasedSampler> (
                beanFactory->configService->getAsInt("percentOfImpressionsToBeWiretapped"),
                "percentOfImpressionsToBeWiretapped"
                );



        adServerWiretapModule->wiretapCassandraService = beanFactory->wiretapCassandraService.get();
        adServerWiretapModule->wiretapImpressionServeResponseSampler = std::move(wiretapImpressionServeResponseSampler);
        //
        auto clickUrlAppenderModule = std::make_shared<ClickUrlAppenderModule>
                                              (beanFactory->entityToModuleStateStats.get());
        //this should be the last module in pipeline


        auto numberOfImpressionsDoneForPixelMatchingPerHour = std::make_shared<gicapods::AtomicLong>();
        auto pixelMatchingUrlAppenderModule =
                std::make_shared<PixelMatchingUrlAppenderModule>(beanFactory->entityToModuleStateStats.get());
        pixelMatchingUrlAppenderModule->numberOfImpressionsDoneForPixelMatchingPerHour = numberOfImpressionsDoneForPixelMatchingPerHour;
        auto creativePalletteCleanerModule =
                std::make_shared<CreativePalletteCleanerModule>(beanFactory->entityToModuleStateStats.get());

        auto impressionTrackerAppenderModule
                = std::make_shared<ImpressionTrackerAppenderModule>(beanFactory->entityToModuleStateStats.get());

        //its set to true, so that adserver doesn't show impressions right away
        //while it has not reloaded data
        auto isReloadingDataInProgress = std::make_shared<gicapods::AtomicBoolean>(true);
        auto numberOfExceptionsInLast10Min = std::make_shared<gicapods::AtomicLong>();

        auto numberOfRealAdServingInLastMinute = std::make_shared<gicapods::AtomicLong>();
        auto numberOfPSAServingInLastHour = std::make_shared<gicapods::AtomicLong>();
        auto adServerHealthService = std::make_shared<AdServerHealthService>();
        adServerHealthService->acceptableNumberOfExceptionsInLastTenMintues = std::make_shared<gicapods::AtomicLong>(
                beanFactory->configService->getAsInt("acceptableNumberOfExceptionsInLastTenMintues"));
        adServerHealthService->isImpressionServerHandlerHealthy = isImpressionServerHandlerHealthy;
        adServerHealthService->isMySqlImpressionMoverHealthy = isMySqlImpressionMoverHealthy;
        adServerHealthService->isSendingPacingInfoModuleHealthy = isSendingPacingInfoModuleHealthy;
        adServerHealthService->isReloadingDataInProgress = isReloadingDataInProgress;
        adServerHealthService->numberOfExceptionsInLast10Min = numberOfExceptionsInLast10Min;
        adServerHealthService->numberOfRealAdServingInLastMinute = numberOfRealAdServingInLastMinute;
        adServerHealthService->numberOfPSAServingInLastHour = numberOfPSAServingInLastHour;
        adServerHealthService->entityToModuleStateStats = beanFactory->entityToModuleStateStats.get();

        std::shared_ptr<AdServer> adserver = std::make_shared<AdServer> ();

        auto eventLogRetriever = std::make_unique<EventLogRetriever>(beanFactory->entityToModuleStateStats.get());
        eventLogRetriever->realTimeEventLogCacheService = beanFactory->realTimeEventLogCacheService.get();

        adserver->adServerHandlerFactory = std::make_unique<AdServerHandlerFactory>
                                                   (beanFactory->targetGroupCacheService,
                                                   beanFactory->creativeCacheService.get(),
                                                   beanFactory->entityToModuleStateStats.get());;

        adserver->adServerHandlerFactory->clickTrackerMainProcessor = std::make_unique<ClickTrackerMainProcessor>();
        adserver->adServerHandlerFactory->clickTrackerMainProcessor->modules.push_back(clickLockModule);
        adserver->adServerHandlerFactory->clickTrackerMainProcessor->modules.push_back(clickPersistentModule);

        adserver->adServerHandlerFactory->impressionTrackerMainProcessor = std::make_unique<ImpressionTrackerMainProcessor>();
        adserver->adServerHandlerFactory->impressionTrackerMainProcessor->creativeCacheService =
                beanFactory->creativeCacheService.get();

        adserver->adServerHandlerFactory->impServMainProcessor = std::make_unique<ImpServMainProcessor> ();
        adserver->adServerHandlerFactory->impServMainProcessor->creativeCacheService = beanFactory->creativeCacheService.get();
        adserver->adServerHandlerFactory->impServMainProcessor->modules.push_back(impressionLockModule);
        adserver->adServerHandlerFactory->impServMainProcessor->modules.push_back(cookiesReaderModule);
        adserver->adServerHandlerFactory->impServMainProcessor->modules.push_back(clientIdModule);
        adserver->adServerHandlerFactory->impServMainProcessor->modules.push_back(adHistoryModule);

        adserver->adServerHandlerFactory->impServMainProcessor->modules.push_back(impressionRecordingModule);
        adserver->adServerHandlerFactory->impServMainProcessor->modules.push_back(cookiesWriterModule);
        adserver->adServerHandlerFactory->impServMainProcessor->modules.push_back(pixelMatchingUrlAppenderModule);
        adserver->adServerHandlerFactory->impServMainProcessor->modules.push_back(impressionTrackerAppenderModule);
        adserver->adServerHandlerFactory->impServMainProcessor->modules.push_back(creativePalletteCleanerModule);

        adserver->adServerHandlerFactory->impServMainProcessor->modules.push_back(clickUrlAppenderModule);
        adserver->adServerHandlerFactory->impServMainProcessor->modules.push_back(confirmWinCounterModule);
        adserver->adServerHandlerFactory->impServMainProcessor->modules.push_back(adservResponseModule);
        adserver->adServerHandlerFactory->impServMainProcessor->modules.push_back(adServerWiretapModule);

        adserver->adServerHandlerFactory->commonRequestHandlerFactory = serviceFactory->commonRequestHandlerFactory.get();
        adserver->adServerHandlerFactory->numberOfExceptionsInLast10Min = numberOfExceptionsInLast10Min;
        adserver->adServerHandlerFactory->numberOfRealAdServingInLastMinute = numberOfRealAdServingInLastMinute;
        adserver->adServerHandlerFactory->numberOfPSAServingInLastHour = numberOfPSAServingInLastHour;
        adserver->adServerHandlerFactory->isImpressionServerHandlerHealthy = isImpressionServerHandlerHealthy;
        adserver->adServerHandlerFactory->adServerHealthService = adServerHealthService.get();
        adserver->adServerHandlerFactory->isReloadingDataInProgress = isReloadingDataInProgress;
        adserver->adServerHandlerFactory->eventLogRetriever = std::move(eventLogRetriever);


        adserver->adServerAsyncJobService = std::make_unique<AdServerAsyncJobService>();
        adserver->adServerAsyncJobService->isSendingPacingInfoModuleHealthy = isSendingPacingInfoModuleHealthy;
        adserver->adServerAsyncJobService->numberOfExceptionsInLast10Min = numberOfExceptionsInLast10Min;
        adserver->adServerAsyncJobService->numberOfRealAdServingInLastMinute = numberOfRealAdServingInLastMinute;
        adserver->adServerAsyncJobService->numberOfPSAServingInLastHour = numberOfPSAServingInLastHour;
        adserver->adServerAsyncJobService->isImpressionServerHandlerHealthy = isImpressionServerHandlerHealthy;
        adserver->adServerAsyncJobService->isMySqlImpressionMoverHealthy = isMySqlImpressionMoverHealthy;
        adserver->adServerAsyncJobService->campaignCacheService = beanFactory->campaignCacheService.get();
        adserver->adServerAsyncJobService->targetGroupCacheService = beanFactory->targetGroupCacheService;
        adserver->adServerAsyncJobService->creativeCacheService = beanFactory->creativeCacheService.get();
        adserver->adServerAsyncJobService->mySqlImpressionMover = beanFactory->mySqlImpressionMover.get();
        adserver->adServerAsyncJobService->mySqlMetricCompressor = beanFactory->mySqlMetricCompressor.get();

        adserver->adServerAsyncJobService->bidderToConfirmedWins = bidderToConfirmedWins;
        adserver->adServerAsyncJobService->configService = beanFactory->configService.get();
        adserver->adServerAsyncJobService->dataReloadService = serviceFactory->dataReloadService.get();
        adserver->adServerAsyncJobService->isReloadingDataInProgress = isReloadingDataInProgress;
        adserver->adServerAsyncJobService->interruptedFlag = interruptedFlag;
        adserver->adServerAsyncJobService->entityToModuleStateStats = beanFactory->entityToModuleStateStats.get();

        int deliveryInfoStatsToPacerInterval =
                ConverterUtil::convertTo<int> (beanFactory->configService->get ("deliveryInfoStatsToPacerInterval"));

        adserver->adServerAsyncJobService->deliveryInfoStatsToPacerInterval = deliveryInfoStatsToPacerInterval;
        assertAndThrow(adserver->adServerAsyncJobService->deliveryInfoStatsToPacerInterval >= 1);

        adserver->adServerAsyncJobService->entityToModuleStateStatsPersistenceService =
                beanFactory->entityToModuleStateStatsPersistenceService.get();
        adserver->adServerAsyncJobService->impressionRecordingModule = impressionRecordingModule.get();

        adserver->configService = beanFactory->configService.get();
        adserver->beanFactory = std::move(beanFactory);

        adserver->run (argc, argv);

        adserver->beanFactory->cassandraDriver->closeSessionAndCluster();

}
