#include "ImpServMainProcessor.h"
#include "EntityToModuleStateStats.h"
#include "SignalHandler.h"
#include "StackTraceUtil.h"
#include "JsonUtil.h"
#include "GUtil.h"
#include "ImpressionServHandler.h"
#include "HttpUtil.h"
#include "DateTimeUtil.h"
#include <boost/exception/all.hpp>
#include "Encoder.h"
#include "AdServContext.h"
#include "EventLog.h"
#include "ImpServMainProcessor.h"
#include "SignalHandler.h"
#include "StackTraceUtil.h"
#include "EventLogRetriever.h"
ImpressionServHandler::ImpressionServHandler() {

}

void ImpressionServHandler::readQueryParams(Poco::URI::QueryParameters &params,
                                            std::string &transactionId,
                                            std::string &winBidPriceFromReq) {
        for (Poco::URI::QueryParameters::const_iterator it = params.begin ();
             it != params.end (); ++it) {

                if (it->first.compare ("transactionId") == 0) {
                        transactionId = it->second;
                        MLOG(3) << "transactionId is set to " << transactionId;
                } else if (it->first.compare ("won") == 0) {
                        winBidPriceFromReq = it->second;
                }
        }

}

void ImpressionServHandler::handleRequest(Poco::Net::HTTPServerRequest &request,
                                          Poco::Net::HTTPServerResponse &response) {

        try {
                //TODO : add a counter to check the difference of numberOfImpressionRequestsProcessed and PROCESS_IMPRESSION_DONE

                Poco::Timestamp now;

                MLOG(10) << "Request from " << request.clientAddress ().toString ();

                process(request, response);

                Poco::Timestamp::TimeDiff diff = now.elapsed (); // how long did it take?

                LOG_EVERY_N(ERROR, 100) << "AdServer Latency  " << diff / 1000 << " milliseconds";

                entityToModuleStateStats->addStateModuleForEntity ("#ImpressionRequestsProcessed",
                                                                   "ImpressionServHandler",
                                                                   "ALL",
                                                                   EntityToModuleStateStats::important);
                return;
        } catch (std::exception const &e) {
                LOG(ERROR)<< "exception while processing impression : " <<e.what();
                //dont add exception msg to the state , it will create too many states
                entityToModuleStateStats->addStateModuleForEntity ("Exception",
                                                                   "ImpressionServHandler",
                                                                   "ALL",
                                                                   EntityToModuleStateStats::exception);

                response.setStatusAndReason (Poco::Net::HTTPResponse::HTTPStatus::HTTP_INTERNAL_SERVER_ERROR,
                                             "error happened on server side");
                std::ostream &ostr = response.send ();
                ostr << ""; //show PSA ad
                numberOfExceptionsInLast10Min->increment();
                return;
        } catch (...) {
                entityToModuleStateStats->addStateModuleForEntity ("ExceptionUnknown",
                                                                   "ImpressionServHandler",
                                                                   "ALL",
                                                                   EntityToModuleStateStats::exception);
                numberOfExceptionsInLast10Min->increment();
        }
}


void ImpressionServHandler::process(Poco::Net::HTTPServerRequest &request,
                                    Poco::Net::HTTPServerResponse &response) {

        std::string winBidPriceFromReq;
        std::string transactionId;
        std::string uriStr = request.getURI ();
        Poco::URI uri (uriStr);
        Poco::URI::QueryParameters params = uri.getQueryParameters ();
        readQueryParams (params, transactionId, winBidPriceFromReq);


        std::shared_ptr<AdServContext> context = std::make_shared<AdServContext>();
        eventLogRetriever->retrieveEventLogFromTransactionId(transactionId, context);

        //these values are very important
        context->requestURI = request.getURI();
        //these values are very important
        context->requestHandlerName = "ImpressionServHandler";
        //these values are very important
        context->adInteractionType = EventLog::EVENT_TYPE_IMPRESSION;

        LOG_EVERY_N(INFO, 100) << google::COUNTER<< "th event :  " << context->eventLog->toJson ();
        context->httpRequestPtr = &request;
        context->httpResponsePtr = &response;

        context->eventLog->winBidPrice = ConverterUtil::convertTo<double>(winBidPriceFromReq);

        MLOG(10) << "context->eventLog->winBidPrice : " << context->eventLog->winBidPrice;

        bool valid = requestValidation (context.get());
        if (!valid) {
                std::ostream &ostr = response.send ();
                LOG(ERROR) << "bad request coming in,  requestValidation failed.";
                ostr << "bad request!";
                return;

        }
        std::string completeAdtag;

        entityToModuleStateStats->addStateModuleForEntity ("SERVING_REAL_AD",
                                                           "ImpressionServHandler",
                                                           "ALL");

        completeAdtag = process (context.get());

        std::ostream &ostr = response.send ();
        LOG_EVERY_N(INFO, 100) << google::COUNTER<<"th response with adtag : " << completeAdtag;

        ostr << completeAdtag;

        incrementPerformanceCounters(context.get());

}
void ImpressionServHandler::incrementPerformanceCounters(AdServContext* context) {
        int targetGroupId = context->eventLog->targetGroupId;

        //very important statistic
        entityToModuleStateStats->addStateModuleForEntity ("PROCESS_IMPRESSION_DONE",
                                                           "ImpressionServHandler",
                                                           EntityToModuleStateStats::all);

        //there is metric in bidder that is called #TG_XX_Bids that is related to this
        entityToModuleStateStats->addStateModuleForEntity ("#TG_"+StringUtil::toStr(targetGroupId)+"_Wins",
                                                           "BidWinTracker",
                                                           EntityToModuleStateStats::all);


}

bool ImpressionServHandler::requestValidation(AdServContext* context) {
        bool result = true;

        return result;

}

std::string ImpressionServHandler::fetchPSA(AdServContext* context) {
        numberOfPSAServingInLastHour->increment();
        return "PSA"; //TODO be completed
}
std::string ImpressionServHandler::process(AdServContext* context) {
        numberOfRealAdServingInLastMinute->increment();
        return impServMainProcessor->process (context);
}
