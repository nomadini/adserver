#ifndef ImpServMainProcessor_H
#define ImpServMainProcessor_H

#include "Status.h"
class AdServerModule;
class TargetGroupCacheService;
#include "AtomicLong.h"
class AdServContext;
#include <memory>
#include <string>
class CreativeCacheService;
class CampaignCacheService;
class EntityToModuleStateStats;
class AdHistoryCassandraService;

class ImpServMainProcessor {

public:
TargetGroupCacheService* targetGroupCacheService;
std::vector<std::shared_ptr<AdServerModule> > modules;

CampaignCacheService* campaignCacheService;
CreativeCacheService* creativeCacheService;
EntityToModuleStateStats* entityToModuleStateStats;
AdHistoryCassandraService* adHistoryCassandraService;

ImpServMainProcessor();

std::string process(AdServContext* context);
virtual ~ImpServMainProcessor();
};



#endif
