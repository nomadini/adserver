
#include "AdHistoryModule.h"
#include "AdServResponseModule.h"
#include "ClientIdModule.h"
#include "CookiesReaderModule.h"
#include "CookiesWriterModule.h"
#include "ImpressionRecordingModule.h"

#include "Status.h"
#include "GUtil.h"
#include "Encoder.h"
#include "AtomicLong.h"
#include "AdServContext.h"
#include "ImpServMainProcessor.h"
#include "ClientIdModule.h"
#include "CampaignCacheService.h"
#include "AdServContext.h"
#include "EventLog.h"
#include "CreativeCacheService.h"
#include "Creative.h"
ImpServMainProcessor::ImpServMainProcessor() {

}

std::string ImpServMainProcessor::process(AdServContext* context) {
        MLOG(3)<<"chosen Creative is : "<<context->eventLog->creativeId;
        context->chosenCreative = creativeCacheService->findByEntityId(context->eventLog->creativeId);

        for (std::vector<std::shared_ptr<AdServerModule> >::iterator it = modules.begin();
             it != modules.end(); ++it) {
                MLOG(10) << "running" << (*it)->getName() << " module";
                (*it)->process(context);
        }

        return context->finalCreativeContent;

}

ImpServMainProcessor::~ImpServMainProcessor() {

}
