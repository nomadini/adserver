#ifndef ImpressionServHandler_H
#define ImpressionServHandler_H

#include <memory>
#include <string>
class AdServContext;
#include "Poco/Net/HTTPServerRequest.h"
#include "Poco/Net/HTTPServerResponse.h"
#include "Poco/Net/HTTPRequestHandler.h"
#include "Poco/URI.h"
class EntityToModuleStateStats;
class SignalHandler;
class EntityToModuleStateStats;
class EventLogRetriever;
class ImpServMainProcessor;
#include <boost/thread.hpp>
#include "AtomicBoolean.h"
#include "AeroCacheService.h"

class ImpressionServHandler : public Poco::Net::HTTPRequestHandler {

public:
AeroCacheService<EventLog>* realTimeEventLogCacheService;
EventLogRetriever* eventLogRetriever;
ImpServMainProcessor* impServMainProcessor;
std::shared_ptr<gicapods::AtomicBoolean> isReloadingDataInProgress;
std::shared_ptr<gicapods::AtomicLong> numberOfExceptionsInLast10Min;
std::shared_ptr<gicapods::AtomicLong> numberOfRealAdServingInLastMinute;
std::shared_ptr<gicapods::AtomicLong> numberOfPSAServingInLastHour;
EntityToModuleStateStats* entityToModuleStateStats;
std::shared_ptr<gicapods::AtomicBoolean> isImpressionServerHandlerHealthy;

ImpressionServHandler();

void readQueryParams(Poco::URI::QueryParameters &params,
                     std::string &transactionId,
                     std::string &winBidPriceFromReq);

void handleRequest(Poco::Net::HTTPServerRequest &request,
                   Poco::Net::HTTPServerResponse &response);

bool requestValidation(AdServContext* opt);


std::string fetchPSA(AdServContext* context);
std::string process(AdServContext* context);
void incrementPerformanceCounters(AdServContext* context);
void process(Poco::Net::HTTPServerRequest &request, Poco::Net::HTTPServerResponse &response);

};

#endif
