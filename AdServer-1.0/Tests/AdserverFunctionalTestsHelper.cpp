
#include "AdServerFunctionalTestsHelper.h"
#include <string>
#include <memory>


std::shared_ptr<AdServer> AdServerFunctionalTestsHelper::adserver;
int AdServerFunctionalTestsHelper::argc;
char** AdServerFunctionalTestsHelper::argv;

    void AdServerFunctionalTestsHelper::init(int argc, char *argv[]) {
        AdServerFunctionalTestsHelper::argc = argc;
        AdServerFunctionalTestsHelper::argv = argv;
    }
