//
// Created by Mahmoud Taabodi on 11/29/15.
//

#ifndef AdServerFunctionalTestsHelper_H
#define AdServerFunctionalTestsHelper_H



#include "AdServer.h"

class AdServerFunctionalTestsHelper {

public:
    static std::shared_ptr<AdServer> adserver;
    static int argc;
    static char** argv;

    static void init(int argc, char *argv[]);
};
#endif //GICAPODS_MODELERFUNCTIONALTESTSHELPER_H
